FROM php:7.2-apache

RUN a2enmod rewrite
RUN docker-php-ext-install pdo pdo_mysql

COPY php.ini /usr/local/etc/php/php.ini
COPY ./web/ /var/www/html/
RUN mkdir /var/www/html/tmp && chmod 777 /var/www/html/tmp
