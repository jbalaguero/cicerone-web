<?php
date_default_timezone_set("Europe/Madrid");
define("KEY_SEED", "c1c3r0n3");
/* <VARIABLES DE CONFIGURACION DE RUTAS> */
//SUBCARPETA
//  Metemos el subdirectorio en el que esta el proyecto.
//  Debe empezar sin / y terminar con /
define("SUBCARPETA", "");
//ROOTPATH
//  Con esta nos ayudaremos para cargar CSS, JS, IMAGENES
define("ROOTPATH", "/" . SUBCARPETA);
define("ROOTPATH_DOMAIN", "/");

if(isset($_SERVER["SERVER_NAME"])){
    if (substr($_SERVER["DOCUMENT_ROOT"], strlen($_SERVER["DOCUMENT_ROOT"]) - 1, strlen($_SERVER["DOCUMENT_ROOT"])) == "/") {
        //ROOTFULLPATH
        //  Esta es la ruta real en el server.
        //  La usaremos en el backend para guardar archivos
        define("ROOTFULLPATH", $_SERVER["DOCUMENT_ROOT"] . SUBCARPETA);
        define("ROOTFULLPATH_DOMAIN", $_SERVER["DOCUMENT_ROOT"]);
    } else {
        //ROOTFULLPATH
        //  Esta es la ruta real en el server.
        //  La usaremos en el backend para guardar archivos
        define("ROOTFULLPATH", $_SERVER["DOCUMENT_ROOT"] . "/" . SUBCARPETA);
        define("ROOTFULLPATH_DOMAIN", $_SERVER["DOCUMENT_ROOT"] . "/" );
    }
}else{
    //ROOTFULLPATH
    //  Esta es la ruta real en el server.
    //  La usaremos en el backend para guardar archivos
    define("ROOTFULLPATH", SUBCARPETA);
    define("ROOTFULLPATH_DOMAIN", "");
}

/* <IDIOMA BACKEND> */
/* Si existe la variable de session cogemos ese valor */
if(isset($_SESSION["PNDR_META_IDIOMA_BACKEND"])){
    define("META_IDIOMA_BACKEND", $_SESSION["PNDR_META_IDIOMA_BACKEND"]);
}else{
    /* Si NO existe la variable de session cogemos el valor de la configuracion */
    define("META_IDIOMA_BACKEND", "es");
}
/* </IDIOMA BACKEND> */

/* <PHPMAILER> */
define("MAIL_HOST", "mail01.ad-6bits.net");
define("MAIL_USER", "info@bdcicerone.com");
define("MAIL_PASS", "-------");
define("MAIL_FROM_EMAIL", "info@bdcicerone.com");
define("MAIL_FROM_NAME", "info@bdcicerone.com");
define("MAIL_PORT", "587");
define("MAIL_SMTP", true);
define("MAIL_SMTP_AUTH", true);
define("MAIL_SMTP_SECURE", "");
define("MAIL_BCC", "");
/* </PHPMAILER> */

define("API_URL", "http://console.bdcicerone.com/API");

/* <VARIABLES BACKEND> */
if (isset($_GET["modulo"])) {
    define("MODULO_BACK", $_GET["modulo"]);
    define("ACCION_BACK", $_GET["accion"]);
    define("ELEMENTO_BACK", $_GET["elemento"]);
}
if (isset($_GET["ajax"])) {
    define("AJAX_BACK", true);
}else{
    define("AJAX_BACK", false);
}
/* </VARIABLES BACKEND> */