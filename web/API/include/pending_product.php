<?php
$app->get('/pending_products', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT CONCAT(supplier.NAME, '#', supplier_product_tmp.SUPPLIER_CODE, ';', supplier_product_tmp.SUPPLIER_PRODUCT_CODE) as SUPPLIER_NAME, SUPPLIER_PRODUCT_CODE, CICERONE_PRODUCT_CODE, DESCRIPTION, SUPPLIER_CITY_NAME, CONCAT(supplier_product_tmp.SUPPLIER_CODE, ';', supplier_product_tmp.SUPPLIER_PRODUCT_CODE) AS SUPPLIER_CODE_SUPPLIER_PRODUCT_CODE  FROM supplier_product_tmp, supplier WHERE supplier_product_tmp.SUPPLIER_CODE = supplier.SUPPLIER_CODE AND supplier_product_tmp.VISIBLE = 'Y' ORDER BY supplier.NAME, SUPPLIER_PRODUCT_CODE ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {
                    return json_encode(array("response" => true, "pending_products" => $datos));
                } else {
                    return json_encode(array("response" => true, "pending_products" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/pending_product/{supplier_code}/{supplier_product_code}', function ($request, $response, array $args) {
    $args['supplier_product_code'] = convert_data_url($args['supplier_product_code']);
    $args['supplier_code']=convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_product_code']);
    $sentencia = $this->db->prepare("SELECT supplier_product_tmp.*, supplier.NAME as SUPPLIER_NAME FROM supplier_product_tmp, supplier WHERE supplier.SUPPLIER_CODE = supplier_product_tmp.SUPPLIER_CODE AND supplier_product_tmp.SUPPLIER_CODE = ? AND supplier_product_tmp.SUPPLIER_PRODUCT_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "pending_product" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/pending_product/{supplier_code}/{supplier_product_code}', function ($request, $response, array $args) {
    $args['supplier_product_code']=convert_data_url($args['supplier_product_code']);
    $args['supplier_code']=convert_data_url($args['supplier_code']);
    $data = $request->getParsedBody();
    $cicerone_product_code = ((filter_var($data['cicerone_product_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['cicerone_product_code'], FILTER_SANITIZE_STRING));
    $supplier_product_code_new = ((filter_var($data['supplier_product_code_new'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['supplier_product_code_new'], FILTER_SANITIZE_STRING));

    if ($cicerone_product_code == "_NEW_PRODUCT_") {
        $cicerone_product_code = $supplier_product_code_new;
    }

    $subsegment_code = ((filter_var($data['subsegment_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['subsegment_code'], FILTER_SANITIZE_STRING));
    $morning_period_code = ((filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING));
    $afternoon_period_code = ((filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING));
    $night_period_code = ((filter_var($data['night_period_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['night_period_code'], FILTER_SANITIZE_STRING));
    $city_code = ((filter_var($data['city_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['city_code'], FILTER_SANITIZE_STRING));
    $description = ((filter_var($data['description'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['description'], FILTER_SANITIZE_STRING));
    $arrayValores = array($cicerone_product_code, $subsegment_code, $morning_period_code, $afternoon_period_code, $night_period_code, $city_code, $description, $args['supplier_code'], $args['supplier_product_code']);
    $sentencia = $this->db->prepare("UPDATE supplier_product_tmp SET CICERONE_PRODUCT_CODE = ?, SUBSEGMENT_CODE = ?, MORNING_PERIOD_CODE = ?, AFTERNOON_PERIOD_CODE = ?, NIGHT_PERIOD_CODE = ?, CITY_CODE = ?, DESCRIPTION = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");

    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/pending_product/{supplier_code}/{supplier_product_code}', function ($request, $response, array $args) {
    $args['supplier_product_code']=convert_data_url($args['supplier_product_code']);
    $args['supplier_code']=convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_product_code']);
    $sentencia = $this->db->prepare("UPDATE supplier_product_tmp SET VISIBLE = 'N' WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/pending_product/move_products', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $codes_to_move = $data['codes_to_move'];
    $contTotal = 0;
    $contOk = 0;
    foreach ($codes_to_move as $code_to_move) {
        $contTotal++;
        $datos_tmp = explode(";", $code_to_move);
        $arrayValores = array($datos_tmp[0], $datos_tmp[1]);

        /* BUSCAMOS EL TMP */
        $sentencia = $this->db->prepare("SELECT * FROM supplier_product_tmp WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");

        if ($sentencia->execute($arrayValores)) {
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {
                $product_tmp = $datos[0];
                /* SI HAY ALGO RELLENO EN CICERONE_PRODUCT_CODE */
                if (trim($product_tmp['CICERONE_PRODUCT_CODE']) != "") {
                    /* SI HAY ALGO RELLENO EN PERIOD */
                    if ($product_tmp['MORNING_PERIOD_CODE'] != NULL || $product_tmp['AFTERNOON_PERIOD_CODE'] != NULL || $product_tmp['NIGHT_PERIOD_CODE'] != NULL) {
                        /* SI HAY ALGO RELLENO EN SUBEGMENT */
                        if ($product_tmp['SUBSEGMENT_CODE'] != NULL) {
                            /* SI HAY ALGO RELLENO EN CITY */
                            if ($product_tmp['CITY_CODE'] != NULL) {
                                $arrayValoresProduct = array($product_tmp['CICERONE_PRODUCT_CODE']);
                                $sentenciaProduct = $this->db->prepare("SELECT * FROM product WHERE PRODUCT_CODE = ?");
                                $sentenciaProduct->execute($arrayValoresProduct);
                                $productos = $sentenciaProduct->fetchAll(PDO::FETCH_ASSOC);
                                if (count($productos) > 0) {
                                    /* SI EL PRODUCTO EXISTE EL MAPPING */
                                    $this->db->beginTransaction();
                                    $arrayValoresMappingNewProduct = array($product_tmp['SUPPLIER_CODE'], $product_tmp['CICERONE_PRODUCT_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                    $sentenciaMappingNewProduct = $this->db->prepare("INSERT INTO product_supplier (SUPPLIER_CODE, PRODUCT_CODE, SUPPLIER_PRODUCT_CODE) VALUES (?,?,?)");
                                    if (!$sentenciaMappingNewProduct) {
                                        $this->db->rollBack();
                                        $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                    } else {
                                        try {
                                            if ($sentenciaMappingNewProduct->execute($arrayValoresMappingNewProduct)) {
                                                /* SI SE GUARDA EL MAPPING ELIMINAMOS EL TMP */
                                                $arrayValoresDeleteTmp = array($product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                $sentenciaDeleteTmp = $this->db->prepare("UPDATE supplier_product_tmp SET VISIBLE = 'N' WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                if (!$sentenciaDeleteTmp) {
                                                    $this->db->rollBack();
                                                    $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                } else {
                                                    try {
                                                        if ($sentenciaDeleteTmp->execute($arrayValoresDeleteTmp)) {
                                                            $this->db->commit();
                                                            $contOk++;
                                                        } else {
                                                            $this->db->rollBack();
                                                            $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                        }
                                                    } catch (Exception $e) {
                                                        $this->db->rollBack();
                                                        $arrayValoresUpdateError = array($e->getMessage(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                    }
                                                }
                                            } else {
                                                $this->db->rollBack();
                                                $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                            }
                                        } catch (Exception $e) {
                                            $this->db->rollBack();
                                            $arrayValoresUpdateError = array($e->getMessage(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                        }
                                    }

                                } else {
                                    /* SI EL PRODUCTO NO ESTA CREADO CREAMOS EL PRODUCTO */
                                    $this->db->beginTransaction();
                                    $arrayValoresNewProducto = array($product_tmp['CICERONE_PRODUCT_CODE'], $product_tmp["DESCRIPTION"], $product_tmp["SUBSEGMENT_CODE"], $product_tmp["CITY_CODE"], $product_tmp["MORNING_PERIOD_CODE"], $product_tmp["AFTERNOON_PERIOD_CODE"], $product_tmp["NIGHT_PERIOD_CODE"]);
                                    $sentenciaNewProducto = $this->db->prepare("INSERT INTO product (PRODUCT_CODE, DESCRIPTION, SUBSEGMENT_CODE, CITY_CODE, MORNING_PERIOD_CODE, AFTERNOON_PERIOD_CODE, NIGHT_PERIOD_CODE) VALUES (?,?,?,?,?,?,?)");
                                    if (!$sentenciaNewProducto) {
                                        $this->db->rollBack();
                                        $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                    } else {
                                        try {
                                            if ($sentenciaNewProducto->execute($arrayValoresNewProducto)) {

                                                /* SI CREAMOS EL PRODUCTO METEMOS EL MAPPING */
                                                $arrayValoresMappingNewProduct = array($product_tmp['SUPPLIER_CODE'], $product_tmp['CICERONE_PRODUCT_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                $sentenciaMappingNewProduct = $this->db->prepare("INSERT INTO product_supplier (SUPPLIER_CODE, PRODUCT_CODE, SUPPLIER_PRODUCT_CODE) VALUES (?,?,?)");
                                                if (!$sentenciaMappingNewProduct) {
                                                    $this->db->rollBack();
                                                    $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                } else {
                                                    try {
                                                        if ($sentenciaMappingNewProduct->execute($arrayValoresMappingNewProduct)) {

                                                            /* SI SE GUARDA EL MAPPING ELIMINAMOS EL TMP */
                                                            $arrayValoresDeleteTmp = array($product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                            $sentenciaDeleteTmp = $this->db->prepare("UPDATE supplier_product_tmp SET VISIBLE = 'N' WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                            if (!$sentenciaDeleteTmp) {
                                                                $this->db->rollBack();
                                                                $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                            } else {
                                                                try {
                                                                    if ($sentenciaDeleteTmp->execute($arrayValoresDeleteTmp)) {
                                                                        $this->db->commit();
                                                                        $contOk++;
                                                                    } else {
                                                                        $this->db->rollBack();
                                                                        $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                                    }
                                                                } catch (Exception $e) {
                                                                    $this->db->rollBack();
                                                                    $arrayValoresUpdateError = array($e->getMessage(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                                }
                                                            }
                                                        } else {
                                                            $this->db->rollBack();
                                                            $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                        }
                                                    } catch (Exception $e) {
                                                        $this->db->rollBack();
                                                        $arrayValoresUpdateError = array($e->getMessage(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                    }
                                                }
                                            } else {
                                                $this->db->rollBack();
                                                $arrayValoresUpdateError = array($this->db->errorInfo(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                            }
                                        } catch (Exception $e) {
                                            $this->db->rollBack();
                                            $arrayValoresUpdateError = array($e->getMessage(), $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                        }
                                    }
                                }
                            } else {
                                $arrayValoresUpdateError = array("Debes seleccionar alguna CITY", $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                            }
                        } else {
                            $arrayValoresUpdateError = array("Debes seleccionar algún SUBSEGMENT", $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                        }
                    } else {
                        $arrayValoresUpdateError = array("Debes seleccionar algún PERIOD", $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                    }
                } else {
                    $arrayValoresUpdateError = array("Debes seleccionar o rellenar un CICERONE_PRODUCT_CODE", $product_tmp['SUPPLIER_CODE'], $product_tmp['SUPPLIER_PRODUCT_CODE']);
                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_product_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                }
            }
        }
    }

    return json_encode(array("response" => true, "message" => $contOk . "/" . $contTotal, "type" => ($contOk == $contTotal ? "success" : (($contOk==0) ? "danger" : "warning"))));
});