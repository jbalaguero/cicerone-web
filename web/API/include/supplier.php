<?php
$app->get('/suppliers', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("
        SELECT supplier.*, GROUP_CONCAT(platform.NAME) as PLATFORMS 
        FROM supplier
        LEFT JOIN supplier_platform ON supplier.SUPPLIER_CODE = supplier_platform.SUPPLIER_CODE
        LEFT JOIN platform ON supplier_platform.PLATFORM_CODE = platform.PLATFORM_CODE
        GROUP BY SUPPLIER_CODE
        ORDER BY supplier.NAME ASC
    ");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "suppliers" => $datos));
                } else {

                    return json_encode(array("response" => true, "suppliers" => array()));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/supplier/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM supplier WHERE SUPPLIER_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "supplier" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/supplier', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $code = filter_var($data['code'], FILTER_SANITIZE_STRING);
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
    $datametrycs_name = filter_var($data['datametrycs_name'], FILTER_SANITIZE_STRING);
    $bi_client_id = filter_var($data['bi_client_id'], FILTER_SANITIZE_STRING);
    $bi_client_secret = filter_var($data['bi_client_secret'], FILTER_SANITIZE_STRING);
    $arrayValores = array($code, $name, $datametrycs_name, $bi_client_id, $bi_client_secret);
    $sentencia = $this->db->prepare("INSERT INTO supplier (SUPPLIER_CODE, NAME, DATAMETRYCS_NAME, BI_CLIENT_ID, BI_CLIENT_SECRET) VALUES (?,?,?,?,?)");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/supplier/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $data = $request->getParsedBody();
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
    $datametrycs_name = filter_var($data['datametrycs_name'], FILTER_SANITIZE_STRING);
    $bi_client_id = filter_var($data['bi_client_id'], FILTER_SANITIZE_STRING);
    $bi_client_secret = filter_var($data['bi_client_secret'], FILTER_SANITIZE_STRING);
    $arrayValores = array($name, $datametrycs_name, $bi_client_id, $bi_client_secret, $args['code']);
    $sentencia = $this->db->prepare("UPDATE supplier SET NAME = ?, DATAMETRYCS_NAME = ?, BI_CLIENT_ID = ?, BI_CLIENT_SECRET = ? WHERE SUPPLIER_CODE = ?");

    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/supplier/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("DELETE FROM supplier WHERE SUPPLIER_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/supplier/{code}/platforms', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("
        SELECT 
        platform.PLATFORM_CODE, 
        COALESCE(supplier_platform.CITY_CODE, '') as CITY_CODE,
        platform.NAME as PLATFORM_NAME, 
        COALESCE(city.DESCRIPTION, '') as CITY_NAME, 
        supplier_platform.CLIENT_ID, 
        supplier_platform.CLIENT_SECRET,
        supplier_platform.API_KEY, 
        REPLACE(supplier_platform.LAST_DAY_PROCESSED, '-', '/') as LAST_DAY_PROCESSED
        FROM supplier_platform
        LEFT JOIN platform ON platform.PLATFORM_CODE = supplier_platform.PLATFORM_CODE 
        LEFT JOIN city ON city.CITY_CODE = supplier_platform.CITY_CODE 
        WHERE supplier_platform.SUPPLIER_CODE = ?
        ORDER BY platform.NAME ASC
    ");

    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "mappings" => $datos));
                } else {

                    return json_encode(array("response" => true, "mappings" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/supplier/{code}/platforms_allowed', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    if ($_GET["for_edit"] == "false") {
        $arrayValores = array($args['code']);
        $sentencia = $this->db->prepare("
        SELECT platform.* 
        FROM platform 
        WHERE PLATFORM_CODE NOT IN (
            SELECT PLATFORM_CODE 
            FROM supplier_platform
            WHERE SUPPLIER_CODE = ? 
        )
        ORDER BY platform.NAME ASC
        ");
    } else {
        $arrayValores = array($args['code'], $_GET["for_edit"]);
        $sentencia = $this->db->prepare("
        SELECT platform.* 
        FROM platform 
        WHERE (PLATFORM_CODE NOT IN (
            SELECT PLATFORM_CODE 
            FROM supplier_platform
            WHERE SUPPLIER_CODE = ? 
        ) OR PLATFORM_CODE = ?)
        ORDER BY platform.NAME ASC
        ");
    }
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "platforms" => $datos));
                } else {

                    return json_encode(array("response" => true, "platforms" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/supplier/{code}/platform/{platform_code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $args['platform_code'] = convert_data_url($args['platform_code']);

    $arrayValores = array($args['platform_code']);
    $sentencia = $this->db->prepare("SELECT * FROM platform WHERE PLATFORM_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        if ($sentencia->execute($arrayValores)) {
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $platform_add = $datos[0];

                $data = $request->getParsedBody();
                $supplier_code = filter_var($args['code'], FILTER_SANITIZE_STRING);
                $platform_code = filter_var($args['platform_code'], FILTER_SANITIZE_STRING);
                $edit_mode = filter_var($data['edit_mode'], FILTER_SANITIZE_STRING);
                $last_platform_code = filter_var($data['last_platform_code'], FILTER_SANITIZE_STRING);
                $city_code = filter_var($data['city_code'], FILTER_SANITIZE_STRING);

                if ($platform_add["PLATFORM_TYPE"] == "A") {
                    $client_id = filter_var($data['client_id'], FILTER_SANITIZE_STRING);
                    $client_secret = filter_var($data['client_secret'], FILTER_SANITIZE_STRING);
                    $api_key = filter_var($data['api_key'], FILTER_SANITIZE_STRING);
                    $last_day_processed = filter_var($data['last_day_processed'], FILTER_SANITIZE_STRING);
                    $last_day_processed_for_date = explode("/", $last_day_processed);
                    $last_day_processed = mktime(0, 0, 0, $last_day_processed_for_date[1], $last_day_processed_for_date[2], $last_day_processed_for_date[0]);
                    $last_day_processed = date("Y-m-d", $last_day_processed);
                } else {
                    $client_id = null;
                    $client_secret = null;
                    $api_key = null;
                    $last_day_processed = null;
                }


                if ($edit_mode == "0") {
                    $arrayValores = array($supplier_code, $platform_code, ($city_code != "" ? $city_code : null), $client_id, $client_secret, $api_key, $last_day_processed);
                    $sentencia = $this->db->prepare("INSERT INTO supplier_platform (SUPPLIER_CODE, PLATFORM_CODE, CITY_CODE, CLIENT_ID, CLIENT_SECRET, API_KEY, LAST_DAY_PROCESSED) VALUES (?,?,?,?,?,?,?)");
                } else {
                    $arrayValores = array($supplier_code, $platform_code, ($city_code != "" ? $city_code : null), $client_id, $client_secret, $api_key, $last_day_processed, $supplier_code, $last_platform_code);
                    $sentencia = $this->db->prepare("UPDATE supplier_platform SET SUPPLIER_CODE=?, PLATFORM_CODE=?, CITY_CODE=?, CLIENT_ID=?, CLIENT_SECRET=?, API_KEY=?, LAST_DAY_PROCESSED=? WHERE SUPPLIER_CODE=? AND PLATFORM_CODE=?");
                }


                if (!$sentencia) {
                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
                } else {
                    try {
                        if ($sentencia->execute($arrayValores)) {

                            return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
                        } else {
                            return $response->withStatus(401)
                                ->withHeader('Content-Type', 'application/json')
                                ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
                        }
                    } catch (Exception $e) {

                        return $response->withStatus(401)
                            ->withHeader('Content-Type', 'application/json')
                            ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
                    }
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
            }
        } else {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
        }
    }

});
$app->delete('/supplier/{code}/platform/{platform_code}', function ($request, $response, array $args) {

    $args['code'] = convert_data_url($args['code']);
    $args['platform_code'] = convert_data_url($args['platform_code']);

    $arrayValores = array($args['code'], $args['platform_code']);

    $sentencia = $this->db->prepare("DELETE FROM supplier_platform WHERE SUPPLIER_CODE = ? AND PLATFORM_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
