<?php
$app->get('/platforms', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT * FROM platform ORDER BY NAME ASC");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "platforms" => $datos));
                } else {

                    return json_encode(array("response" => true, "platforms" => array()));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/platform/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM platform WHERE PLATFORM_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "platform" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/platform', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $code = filter_var($data['code'], FILTER_SANITIZE_STRING);
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
    $platform_type = filter_var($data['platform_type'], FILTER_SANITIZE_STRING);
    if ($platform_type == "A") {
        $url_authentication = filter_var($data['url_authentication'], FILTER_SANITIZE_STRING);
        $url_bookings = filter_var($data['url_bookings'], FILTER_SANITIZE_STRING);
        $url_products = filter_var($data['url_products'], FILTER_SANITIZE_STRING);
    } else {
        $url_authentication = null;
        $url_bookings = null;
        $url_products = null;
    }
    $date_format = filter_var($data['date_format'], FILTER_SANITIZE_STRING);
    $arrayValores = array($code, $name, $platform_type, $url_authentication, $url_bookings, $url_products, $date_format);
    $sentencia = $this->db->prepare("INSERT INTO platform (PLATFORM_CODE, NAME, PLATFORM_TYPE, URL_AUTHENTICATION, URL_BOOKINGS, URL_PRODUCTS, DATE_FORMAT) VALUES (?,?,?,?,?,?,?)");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/platform/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);

    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM platform WHERE PLATFORM_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    $data = $request->getParsedBody();
                    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
                    if ($datos[0]["PLATFORM_TYPE"] == "A") {
                        $url_authentication = filter_var($data['url_authentication'], FILTER_SANITIZE_STRING);
                        $url_bookings = filter_var($data['url_bookings'], FILTER_SANITIZE_STRING);
                        $url_products = filter_var($data['url_products'], FILTER_SANITIZE_STRING);
                    } else {
                        $url_authentication = null;
                        $url_bookings = null;
                        $url_products = null;
                    }
                    $date_format = filter_var($data['date_format'], FILTER_SANITIZE_STRING);
                    $arrayValores = array($name, $url_authentication, $url_bookings, $url_products, $date_format, $args['code']);
                    $sentencia = $this->db->prepare("UPDATE platform SET NAME = ?, URL_AUTHENTICATION = ?, URL_BOOKINGS = ?, URL_PRODUCTS = ?, DATE_FORMAT = ? WHERE PLATFORM_CODE = ?");

                    if (!$sentencia) {
                        return $response->withStatus(401)
                            ->withHeader('Content-Type', 'application/json')
                            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
                    } else {
                        try {
                            if ($sentencia->execute($arrayValores)) {

                                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
                            } else {


                                return $response->withStatus(401)
                                    ->withHeader('Content-Type', 'application/json')
                                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
                            }
                        } catch (Exception $e) {

                            return $response->withStatus(401)
                                ->withHeader('Content-Type', 'application/json')
                                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
                        }
                    }
                } else {
                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }


});
$app->delete('/platform/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("DELETE FROM platform WHERE PLATFORM_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});