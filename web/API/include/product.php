<?php
$app->get('/products', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT product.PRODUCT_CODE, product.DESCRIPTION, subsegment.DESCRIPTION as SUBSEGMENT_DESCRIPTION, city.DESCRIPTION as CITY_DESCRIPTION FROM product, subsegment, city WHERE product.SUBSEGMENT_CODE = subsegment.SUBSEGMENT_CODE AND product.CITY_CODE = city.CITY_CODE ORDER BY product.DESCRIPTION ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "products" => $datos));
                } else {

                    return json_encode(array("response" => true, "products" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/products/for_select', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT product.PRODUCT_CODE, CONCAT(product.PRODUCT_CODE, ' :: ', product.DESCRIPTION) as DESCRIPTION, subsegment.DESCRIPTION as SUBSEGMENT_DESCRIPTION, city.DESCRIPTION as CITY_DESCRIPTION FROM product, subsegment, city WHERE product.SUBSEGMENT_CODE = subsegment.SUBSEGMENT_CODE AND product.CITY_CODE = city.CITY_CODE ORDER BY product.PRODUCT_CODE ASC, product.DESCRIPTION ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "products" => $datos));
                } else {

                    return json_encode(array("response" => true, "products" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/product/{code}', function ($request, $response, array $args) {
    $args['code']=convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM product WHERE PRODUCT_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "product" => $datos[0]));
                } else {


            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/product', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $code = filter_var($data['code'], FILTER_SANITIZE_STRING);
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $subsegment_code = filter_var($data['subsegment_code'], FILTER_SANITIZE_STRING);
    $city_code = filter_var($data['city_code'], FILTER_SANITIZE_STRING);
    $morning_period_code = ((filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING)=="") ? NULL : filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING));
    $afternoon_period_code = ((filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING)=="") ? NULL : filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING));
    $night_period_code = ((filter_var($data['night_period_code'], FILTER_SANITIZE_STRING)=="") ? NULL : filter_var($data['night_period_code'], FILTER_SANITIZE_STRING));
    $arrayValores = array($code, $description, $subsegment_code, $city_code, $morning_period_code, $afternoon_period_code, $night_period_code);
    $sentencia = $this->db->prepare("INSERT INTO product (PRODUCT_CODE, DESCRIPTION, SUBSEGMENT_CODE, CITY_CODE, MORNING_PERIOD_CODE, AFTERNOON_PERIOD_CODE, NIGHT_PERIOD_CODE) VALUES (?,?,?,?,?,?,?)");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/product/{code}', function ($request, $response, array $args) {

    $args['code']=convert_data_url($args['code']);
    $data = $request->getParsedBody();
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $morning_period_code = ((filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING)=="") ? NULL : filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING));
    $afternoon_period_code = ((filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING)=="") ? NULL : filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING));
    $night_period_code = ((filter_var($data['night_period_code'], FILTER_SANITIZE_STRING)=="") ? NULL : filter_var($data['night_period_code'], FILTER_SANITIZE_STRING));
    $arrayValores = array($description, $morning_period_code, $afternoon_period_code, $night_period_code, $args['code']);
    $sentencia = $this->db->prepare("UPDATE product SET DESCRIPTION = ?, MORNING_PERIOD_CODE = ?, AFTERNOON_PERIOD_CODE = ?, NIGHT_PERIOD_CODE = ? WHERE PRODUCT_CODE = ?");

    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/product/{code}', function ($request, $response, array $args) {

    $args['code']=convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("DELETE FROM product WHERE PRODUCT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/product/{code}/suppliers', function ($request, $response, array $args) {

    $args['code']=convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT supplier.NAME as SUPPLIER_NAME, product_supplier.SUPPLIER_PRODUCT_CODE, CONCAT(product_supplier.SUPPLIER_CODE, '-', product_supplier.SUPPLIER_PRODUCT_CODE) as SUPPLIER_CODE_SUPPLIER_PRODUCT_CODE FROM product_supplier, supplier WHERE supplier.SUPPLIER_CODE = product_supplier.SUPPLIER_CODE AND product_supplier.PRODUCT_CODE = ? ORDER BY supplier.NAME ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "mappings" => $datos));
                } else {

                    return json_encode(array("response" => true, "mappings" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/product/{code}/suppliers_allowed', function ($request, $response, array $args) {

    $args['code']=convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT supplier.* FROM supplier ORDER BY supplier.NAME ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "suppliers" => $datos));
                } else {

                    return json_encode(array("response" => true, "suppliers" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/product/{code}/supplier/{supplier_code}', function ($request, $response, array $args) {

    $args['code']=convert_data_url($args['code']);
    $args['supplier_code']=convert_data_url($args['supplier_code']);

    $data = $request->getParsedBody();
    $code = $args['code'];
    $supplier_code = $args['supplier_code'];
    $supplier_product_code = filter_var($data['supplier_product_code'], FILTER_SANITIZE_STRING);
    $arrayValores = array($supplier_code, $code, $supplier_product_code);
    $sentencia = $this->db->prepare("INSERT INTO product_supplier (SUPPLIER_CODE, PRODUCT_CODE, SUPPLIER_PRODUCT_CODE) VALUES (?,?,?)");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/product/{code}/supplier/{supplier_code}', function ($request, $response, array $args) {

    $args['code']=convert_data_url($args['code']);
    $args['supplier_code']=convert_data_url($args['supplier_code']);
    $data = $request->getParsedBody();
    $supplier_product_code = filter_var($data['supplier_product_code'], FILTER_SANITIZE_STRING);
    $arrayValores = array($args['code'], $args['supplier_code'], $supplier_product_code);
    $sentencia = $this->db->prepare("DELETE FROM product_supplier WHERE PRODUCT_CODE = ? AND SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});