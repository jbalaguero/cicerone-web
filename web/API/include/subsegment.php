<?php
$app->get('/subsegments', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT subsegment.*, segment.DESCRIPTION as SEGMENT_DESCRIPTION  FROM subsegment, segment WHERE segment.SEGMENT_CODE = subsegment.SEGMENT_CODE ORDER BY DESCRIPTION ASC");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "subsegments" => $datos));
                } else {

                    return json_encode(array("response" => true, "subsegments" => array()));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/subsegment/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM subsegment WHERE SUBSEGMENT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "subsegment" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/subsegment', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $code = filter_var($data['code'], FILTER_SANITIZE_STRING);
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $segment_code = filter_var($data['segment_code'], FILTER_SANITIZE_STRING);
    $arrayValores = array($code, $description, $segment_code);
    $sentencia = $this->db->prepare("INSERT INTO subsegment (SUBSEGMENT_CODE, DESCRIPTION, SEGMENT_CODE) VALUES (?,?,?)");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/subsegment/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);

    $data = $request->getParsedBody();
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $arrayValores = array($description, $args['code']);
    $sentencia = $this->db->prepare("UPDATE subsegment SET DESCRIPTION = ? WHERE SUBSEGMENT_CODE = ?");

    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/subsegment/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("DELETE FROM subsegment WHERE SUBSEGMENT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});