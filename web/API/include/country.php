<?php
$app->get('/countries', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT * FROM country ORDER BY DESCRIPTION ASC");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "countries" => $datos));
                } else {

                    return json_encode(array("response" => true, "countries" => array()));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/country/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM country WHERE COUNTRY_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "country" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/country', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $code = filter_var($data['code'], FILTER_SANITIZE_STRING);
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $include_in_search = (isset($data['include_in_search']) ? 1 : 0);
    $arrayValores = array($code, $description, $include_in_search);
    $sentencia = $this->db->prepare("INSERT INTO country (COUNTRY_CODE, DESCRIPTION, INCLUDE_IN_SEARCH) VALUES (?,?,?)");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/country/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);

    $data = $request->getParsedBody();
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $include_in_search = (isset($data['include_in_search']) ? 1 : 0);

    $arrayValores = array($description, $include_in_search, $args['code']);
    $sentencia = $this->db->prepare("UPDATE country SET DESCRIPTION = ?, INCLUDE_IN_SEARCH = ? WHERE COUNTRY_CODE = ?");

    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/country/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("DELETE FROM country WHERE COUNTRY_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/country/{code}/suppliers', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT supplier.NAME as SUPPLIER_NAME, country_supplier.SUPPLIER_COUNTRY_CODE, country_supplier.SUPPLIER_CODE FROM country_supplier, supplier WHERE supplier.SUPPLIER_CODE = country_supplier.SUPPLIER_CODE AND country_supplier.COUNTRY_CODE = ? ORDER BY supplier.NAME ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "mappings" => $datos));
                } else {

                    return json_encode(array("response" => true, "mappings" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/country/{code}/suppliers_allowed', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT supplier.* FROM supplier WHERE supplier.SUPPLIER_CODE NOT IN (SELECT country_supplier.SUPPLIER_CODE FROM country_supplier WHERE country_supplier.COUNTRY_CODE = ?) ORDER BY supplier.NAME ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "suppliers" => $datos));
                } else {

                    return json_encode(array("response" => true, "suppliers" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/country/{code}/supplier/{supplier_code}', function ($request, $response, array $args) {

    $args['code'] = convert_data_url($args['code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);

    $data = $request->getParsedBody();
    $code = $args['code'];
    $supplier_code = $args['supplier_code'];
    $supplier_country_code = filter_var($data['supplier_country_code'], FILTER_SANITIZE_STRING);
    $arrayValores = array($supplier_code, $code, $supplier_country_code);
    $sentencia = $this->db->prepare("INSERT INTO country_supplier (SUPPLIER_CODE, COUNTRY_CODE, SUPPLIER_COUNTRY_CODE) VALUES (?,?,?)");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/country/{code}/supplier/{supplier_code}', function ($request, $response, array $args) {

    $args['code'] = convert_data_url($args['code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $data = $request->getParsedBody();
    $arrayValores = array($args['code'], $args['supplier_code']);
    $sentencia = $this->db->prepare("DELETE FROM country_supplier WHERE COUNTRY_CODE = ? AND SUPPLIER_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});