<?php
$app->get('/removed_agents', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT CONCAT(supplier.NAME, '#', supplier_agent_tmp.SUPPLIER_CODE, ';', supplier_agent_tmp.SUPPLIER_AGENT_CODE) as SUPPLIER_NAME, SUPPLIER_AGENT_CODE, SUPPLIER_AGENT_NAME, CICERONE_AGENT_CODE, CONCAT(supplier_agent_tmp.SUPPLIER_CODE, ';', supplier_agent_tmp.SUPPLIER_AGENT_CODE) AS SUPPLIER_CODE_SUPPLIER_AGENT_CODE  FROM supplier_agent_tmp, supplier WHERE supplier_agent_tmp.SUPPLIER_CODE = supplier.SUPPLIER_CODE AND supplier_agent_tmp.VISIBLE = 'N'  ORDER BY supplier.NAME, SUPPLIER_AGENT_CODE ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {
                    return json_encode(array("response" => true, "removed_agents" => $datos));
                } else {
                    return json_encode(array("response" => true, "removed_agents" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/removed_agent/{supplier_code}/{supplier_agent_code}', function ($request, $response, array $args) {
    $args['supplier_agent_code'] = convert_data_url($args['supplier_agent_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_agent_code']);
    $sentencia = $this->db->prepare("SELECT supplier_agent_tmp.*, supplier.NAME as SUPPLIER_NAME FROM supplier_agent_tmp, supplier WHERE supplier.SUPPLIER_CODE = supplier_agent_tmp.SUPPLIER_CODE AND supplier_agent_tmp.SUPPLIER_CODE = ? AND supplier_agent_tmp.SUPPLIER_AGENT_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "removed_agent" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/removed_agent/{supplier_code}/{supplier_agent_code}', function ($request, $response, array $args) {
    $args['supplier_agent_code'] = convert_data_url($args['supplier_agent_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $data = $request->getParsedBody();
    $cicerone_agent_code = ((filter_var($data['cicerone_agent_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['cicerone_agent_code'], FILTER_SANITIZE_STRING));
    $supplier_agent_code_new = ((filter_var($data['supplier_agent_code_new'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['supplier_agent_code_new'], FILTER_SANITIZE_STRING));
    $supplier_agent_name = ((filter_var($data['supplier_agent_name'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['supplier_agent_name'], FILTER_SANITIZE_STRING));

    if ($cicerone_agent_code == "_NEW_AGENT_") {
        $cicerone_agent_code = $supplier_agent_code_new;
    }

    $arrayValores = array($cicerone_agent_code, $supplier_agent_name, $args['supplier_code'], $args['supplier_agent_code']);
    $sentencia = $this->db->prepare("
        UPDATE supplier_agent_tmp 
        SET CICERONE_AGENT_CODE = ?,
        SUPPLIER_AGENT_NAME = ?
        WHERE SUPPLIER_CODE = ? 
        AND SUPPLIER_AGENT_CODE = ?");

    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/removed_agent/{supplier_code}/{supplier_agent_code}', function ($request, $response, array $args) {
    $args['supplier_agent_code'] = convert_data_url($args['supplier_agent_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_agent_code']);
    $sentencia = $this->db->prepare("UPDATE supplier_agent_tmp SET VISIBLE = 'Y' WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});