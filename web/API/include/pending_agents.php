<?php
$app->get('/pending_agents', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT CONCAT(supplier.NAME, '#', supplier_agent_tmp.SUPPLIER_CODE, ';', supplier_agent_tmp.SUPPLIER_AGENT_CODE) as SUPPLIER_NAME, SUPPLIER_AGENT_CODE, SUPPLIER_AGENT_NAME, CICERONE_AGENT_CODE, CONCAT(supplier_agent_tmp.SUPPLIER_CODE, ';', supplier_agent_tmp.SUPPLIER_AGENT_CODE) AS SUPPLIER_CODE_SUPPLIER_AGENT_CODE  FROM supplier_agent_tmp, supplier WHERE supplier_agent_tmp.SUPPLIER_CODE = supplier.SUPPLIER_CODE AND VISIBLE = 'Y'  ORDER BY supplier.NAME, SUPPLIER_AGENT_CODE ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {
                    return json_encode(array("response" => true, "pending_agents" => $datos));
                } else {
                    return json_encode(array("response" => true, "pending_agents" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/pending_agent/{supplier_code}/{supplier_agent_code}', function ($request, $response, array $args) {
    $args['supplier_agent_code'] = convert_data_url($args['supplier_agent_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_agent_code']);
    $sentencia = $this->db->prepare("SELECT supplier_agent_tmp.*, supplier.NAME as SUPPLIER_NAME FROM supplier_agent_tmp, supplier WHERE supplier.SUPPLIER_CODE = supplier_agent_tmp.SUPPLIER_CODE AND supplier_agent_tmp.SUPPLIER_CODE = ? AND supplier_agent_tmp.SUPPLIER_AGENT_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "pending_agent" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/pending_agent/{supplier_code}/{supplier_agent_code}', function ($request, $response, array $args) {
    $args['supplier_agent_code'] = convert_data_url($args['supplier_agent_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $data = $request->getParsedBody();
    $cicerone_agent_code = ((filter_var($data['cicerone_agent_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['cicerone_agent_code'], FILTER_SANITIZE_STRING));
    $supplier_agent_code_new = ((filter_var($data['supplier_agent_code_new'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['supplier_agent_code_new'], FILTER_SANITIZE_STRING));
    $supplier_agent_name = ((filter_var($data['supplier_agent_name'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['supplier_agent_name'], FILTER_SANITIZE_STRING));

    if ($cicerone_agent_code == "_NEW_AGENT_") {
        $cicerone_agent_code = $supplier_agent_code_new;
    }

    $arrayValores = array($cicerone_agent_code, $supplier_agent_name, $args['supplier_code'], $args['supplier_agent_code']);
    $sentencia = $this->db->prepare("
        UPDATE supplier_agent_tmp 
        SET CICERONE_AGENT_CODE = ?,
        SUPPLIER_AGENT_NAME = ?
        WHERE SUPPLIER_CODE = ? 
        AND SUPPLIER_AGENT_CODE = ?");

    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/pending_agent/{supplier_code}/{supplier_agent_code}', function ($request, $response, array $args) {
    $args['supplier_agent_code'] = convert_data_url($args['supplier_agent_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_agent_code']);
    $sentencia = $this->db->prepare("UPDATE supplier_agent_tmp SET VISIBLE = 'N' WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/pending_agent/move_agents', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $codes_to_move = $data['codes_to_move'];
    $contTotal = 0;
    $contOk = 0;
    foreach ($codes_to_move as $code_to_move) {
        $contTotal++;
        $datos_tmp = explode(";", $code_to_move);
        $arrayValores = array($datos_tmp[0], $datos_tmp[1]);

        /* BUSCAMOS EL TMP */
        $sentencia = $this->db->prepare("SELECT * FROM supplier_agent_tmp WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");

        if ($sentencia->execute($arrayValores)) {
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {
                $agent_tmp = $datos[0];
                /* SI HAY ALGO RELLENO EN CICERONE_AGENT_CODE */
                if (trim($agent_tmp['CICERONE_AGENT_CODE']) != "") {
                    $arrayValoresAgen = array($agent_tmp['CICERONE_AGENT_CODE']);
                    $sentenciaAgent = $this->db->prepare("SELECT * FROM agent WHERE AGENT_CODE = ?");
                    $sentenciaAgent->execute($arrayValoresAgen);
                    $agentes = $sentenciaAgent->fetchAll(PDO::FETCH_ASSOC);
                    if (count($agentes) > 0) {
                        /* SI EL AGENTE EXISTE EL MAPPING */
                        $this->db->beginTransaction();
                        $arrayValoresMappingNewAgent = array($agent_tmp['SUPPLIER_CODE'], $agent_tmp['CICERONE_AGENT_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                        $sentenciaMappingNewAgent = $this->db->prepare("INSERT INTO agent_supplier (SUPPLIER_CODE, AGENT_CODE, SUPPLIER_AGENT_CODE) VALUES (?,?,?)");
                        if (!$sentenciaMappingNewAgent) {
                            $this->db->rollBack();
                            $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                        } else {
                            try {
                                if ($sentenciaMappingNewAgent->execute($arrayValoresMappingNewAgent)) {
                                    /* SI SE GUARDA EL MAPPING ELIMINAMOS EL TMP */
                                    $arrayValoresDeleteTmp = array($agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                    $sentenciaDeleteTmp = $this->db->prepare("UPDATE supplier_agent_tmp SET VISIBLE = 'N' WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                    if (!$sentenciaDeleteTmp) {
                                        $this->db->rollBack();
                                        $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                    } else {
                                        try {
                                            if ($sentenciaDeleteTmp->execute($arrayValoresDeleteTmp)) {
                                                $this->db->commit();
                                                $contOk++;
                                            } else {
                                                $this->db->rollBack();
                                                $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                            }
                                        } catch (Exception $e) {
                                            $this->db->rollBack();
                                            $arrayValoresUpdateError = array($e->getMessage(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                        }
                                    }
                                } else {
                                    $this->db->rollBack();
                                    $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                }
                            } catch (Exception $e) {
                                $this->db->rollBack();
                                $arrayValoresUpdateError = array($e->getMessage(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                            }
                        }

                    } else {
                        /* SI EL AGENTE NO ESTA CREADO CREAMOS EL AGENTE */
                        $this->db->beginTransaction();
                        $arrayValoresNewAgente = array($agent_tmp['CICERONE_AGENT_CODE'], $agent_tmp["SUPPLIER_AGENT_NAME"]);
                        $sentenciaNewAgente = $this->db->prepare("INSERT INTO agent (AGENT_CODE, NAME) VALUES (?,?)");
                        if (!$sentenciaNewAgente) {
                            $this->db->rollBack();
                            $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                        } else {
                            try {
                                if ($sentenciaNewAgente->execute($arrayValoresNewAgente)) {

                                    /* SI CREAMOS EL AGENTE METEMOS EL MAPPING */
                                    $arrayValoresMappingNewAgent = array($agent_tmp['SUPPLIER_CODE'], $agent_tmp['CICERONE_AGENT_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                    $sentenciaMappingNewAgent = $this->db->prepare("INSERT INTO agent_supplier (SUPPLIER_CODE, AGENT_CODE, SUPPLIER_AGENT_CODE) VALUES (?,?,?)");
                                    if (!$sentenciaMappingNewAgent) {
                                        $this->db->rollBack();
                                        $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                    } else {
                                        try {
                                            if ($sentenciaMappingNewAgent->execute($arrayValoresMappingNewAgent)) {

                                                /* SI SE GUARDA EL MAPPING ELIMINAMOS EL TMP */
                                                $arrayValoresDeleteTmp = array($agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                                $sentenciaDeleteTmp = $this->db->prepare("UPDATE supplier_agent_tmp SET VISIBLE = 'N' WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                                if (!$sentenciaDeleteTmp) {
                                                    $this->db->rollBack();
                                                    $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                } else {
                                                    try {
                                                        if ($sentenciaDeleteTmp->execute($arrayValoresDeleteTmp)) {
                                                            $this->db->commit();
                                                            $contOk++;
                                                        } else {
                                                            $this->db->rollBack();
                                                            $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                        }
                                                    } catch (Exception $e) {
                                                        $this->db->rollBack();
                                                        $arrayValoresUpdateError = array($e->getMessage(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                                        $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                                        $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                                    }
                                                }
                                            } else {
                                                $this->db->rollBack();
                                                $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                            }
                                        } catch (Exception $e) {
                                            $this->db->rollBack();
                                            $arrayValoresUpdateError = array($e->getMessage(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                            $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                            $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                        }
                                    }
                                } else {
                                    $this->db->rollBack();
                                    $arrayValoresUpdateError = array($this->db->errorInfo(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                                }
                            } catch (Exception $e) {
                                $this->db->rollBack();
                                $arrayValoresUpdateError = array($e->getMessage(), $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                                $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                                $sentenciaUpdateError->execute($arrayValoresUpdateError);
                            }
                        }
                    }
                } else {
                    $arrayValoresUpdateError = array("Debes seleccionar o rellenar un CICERONE_AGENT_CODE", $agent_tmp['SUPPLIER_CODE'], $agent_tmp['SUPPLIER_AGENT_CODE']);
                    $sentenciaUpdateError = $this->db->prepare("UPDATE supplier_agent_tmp SET ERROR = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_AGENT_CODE = ?");
                    $sentenciaUpdateError->execute($arrayValoresUpdateError);
                }
            }
        }
    }

    return json_encode(array("response" => true, "message" => $contOk . "/" . $contTotal, "type" => ($contOk == $contTotal ? "success" : (($contOk == 0) ? "danger" : "warning"))));
});