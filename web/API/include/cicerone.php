<?php
$app->get('/cicerone/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM cicerone WHERE COMPANY_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "cicerone" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/cicerone/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);

    $data = $request->getParsedBody();
    $client_id = filter_var($data['client_id'], FILTER_SANITIZE_STRING);
    $client_secret = filter_var($data['client_secret'], FILTER_SANITIZE_STRING);
    $dm_url_login = filter_var($data['dm_url_login'], FILTER_SANITIZE_STRING);
    $dm_url_audit = filter_var($data['dm_url_audit'], FILTER_SANITIZE_STRING);
    $api_code = filter_var($data['api_code'], FILTER_SANITIZE_STRING);
    $method_code = filter_var($data['method_code'], FILTER_SANITIZE_STRING);
    $smtp_server = filter_var($data['smtp_server'], FILTER_SANITIZE_STRING);
    $smtp_port = filter_var($data['smtp_port'], FILTER_SANITIZE_STRING);
    $smtp_from = filter_var($data['smtp_from'], FILTER_SANITIZE_STRING);
    $smtp_to = filter_var($data['smtp_to'], FILTER_SANITIZE_STRING);
    $smtp_user = filter_var($data['smtp_user'], FILTER_SANITIZE_STRING);
    $smtp_password = filter_var($data['smtp_password'], FILTER_SANITIZE_STRING);
    $arrayValores = array($client_id, $client_secret, $dm_url_login, $dm_url_audit, $api_code, $smtp_server, $smtp_port, $smtp_from, $smtp_to, $smtp_user, $smtp_password, $args['code']);
    $sentencia = $this->db->prepare("UPDATE cicerone SET CLIENT_ID = ?, CLIENT_SECRET = ?, DM_URL_LOGIN = ?, DM_URL_AUDIT = ?, API_CODE = ?, SMTP_SERVER = ?, SMTP_PORT = ?, SMTP_FROM = ?, SMTP_TO = ?, SMTP_USER = ?, SMTP_PASSWORD = ?  WHERE COMPANY_CODE = ?");

    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/export', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $filesToZip = array();
    if ($data != NULL) {
        if (file_exists('../temp/export_suppliers.txt')) {
            unlink('../temp/export_suppliers.txt');
        }
        if (file_exists('../temp/export_agents.txt')) {
            unlink('../temp/export_agents.txt');
        }
        if (file_exists('../temp/export_countries.txt')) {
            unlink('../temp/export_countries.txt');
        }
        if (file_exists('../temp/export_nationalities.txt')) {
            unlink('../temp/export_nationalities.txt');
        }
        if (file_exists('../temp/export_cities.txt')) {
            unlink('../temp/export_cities.txt');
        }
        if (file_exists('../temp/export_currencies.txt')) {
            unlink('../temp/export_currencies.txt');
        }
        if (file_exists('../temp/export_segments.txt')) {
            unlink('../temp/export_segments.txt');
        }
        if (file_exists('../temp/export_subsegments.txt')) {
            unlink('../temp/export_subsegments.txt');
        }
        if (file_exists('../temp/export_products.txt')) {
            unlink('../temp/export_products.txt');
        }
        if (file_exists('../temp/export.zip')) {
            unlink('../temp/export.zip');
        }

        if (isset($data["export_suppliers"])) {
            $sentencia = $this->db->prepare("SELECT SUPPLIER_CODE, CONCAT(NAME,'|',DATAMETRYCS_NAME) FROM supplier");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_suppliers.txt', 'w');
                $filesToZip[] = '../temp/export_suppliers.txt';
                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_agents"])) {
            $sentencia = $this->db->prepare("SELECT AGENT_CODE, NAME FROM agent");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_agents.txt', 'w');
                $filesToZip[] = '../temp/export_agents.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_countries"])) {
            $sentencia = $this->db->prepare("SELECT COUNTRY_CODE, DESCRIPTION FROM country WHERE INCLUDE_IN_SEARCH=1");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_countries.txt', 'w');
                $filesToZip[] = '../temp/export_countries.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_nationalities"])) {
            $sentencia = $this->db->prepare("SELECT COUNTRY_CODE, DESCRIPTION FROM country");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_nationalities.txt', 'w');
                $filesToZip[] = '../temp/export_nationalities.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_cities"])) {
            $sentencia = $this->db->prepare("SELECT CITY_CODE, DESCRIPTION FROM city");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_cities.txt', 'w');
                $filesToZip[] = '../temp/export_cities.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_currencies"])) {
            $sentencia = $this->db->prepare("SELECT CURRENCY_CODE, DESCRIPTION FROM currency");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_currencies.txt', 'w');
                $filesToZip[] = '../temp/export_currencies.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_segments"])) {
            $sentencia = $this->db->prepare("SELECT SEGMENT_CODE, DESCRIPTION FROM segment");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_segments.txt', 'w');
                $filesToZip[] = '../temp/export_segments.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_subsegments"])) {
            $sentencia = $this->db->prepare("SELECT SUBSEGMENT_CODE, DESCRIPTION FROM subsegment");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_subsegments.txt', 'w');
                $filesToZip[] = '../temp/export_subsegments.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }
        if (isset($data["export_products"])) {
            $sentencia = $this->db->prepare("SELECT PRODUCT_CODE, DESCRIPTION FROM product");
            $sentencia->execute();
            $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            if (count($datos) > 0) {

                $fp = fopen('../temp/export_products.txt', 'w');
                $filesToZip[] = '../temp/export_products.txt';

                foreach ($datos as $fields) {
                    fwrite($fp, implode(chr(9), $fields) . PHP_EOL);
                }

                fclose($fp);
            }

        }

        if (count($filesToZip) > 0) {
            $zip = new ZipArchive;
            if ($zip->open('../temp/export.zip', ZipArchive::CREATE) === TRUE) {
                foreach ($filesToZip as $fileToZip) {
                    $zip->addFile($fileToZip);
                }
                $zip->close();
            }
            return json_encode(array("response" => true, "message" => "/temp/export.zip"));
        } else {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => "t(NO_HAY_DATOS_QUE_EXPORTAR)")));
        }

    } else {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => "t(DEBES_SELECCIONAR_ALGUNA_OPCIONES)")));
    }
});
$app->post('/login', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $user = filter_var($data['user'], FILTER_SANITIZE_STRING);
    $password = filter_var($data['password'], FILTER_SANITIZE_STRING);
    //$arrayValores=array($user, md5($password));
    $arrayValores = array($user, $password);
    $sentencia = $this->db->prepare("SELECT * FROM cicerone WHERE CLIENT_ID = ? AND CLIENT_SECRET = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "user" => $datos[0]));
                } else {

                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "User/Password erroneos")));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});