<?php
$app->get('/removed_products', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT CONCAT(supplier.NAME, '#', supplier_product_tmp.SUPPLIER_CODE, ';', supplier_product_tmp.SUPPLIER_PRODUCT_CODE) as SUPPLIER_NAME, SUPPLIER_PRODUCT_CODE, CICERONE_PRODUCT_CODE, DESCRIPTION, SUPPLIER_CITY_NAME, CONCAT(supplier_product_tmp.SUPPLIER_CODE, ';', supplier_product_tmp.SUPPLIER_PRODUCT_CODE) AS SUPPLIER_CODE_SUPPLIER_PRODUCT_CODE  FROM supplier_product_tmp, supplier WHERE supplier_product_tmp.SUPPLIER_CODE = supplier.SUPPLIER_CODE AND supplier_product_tmp.VISIBLE = 'N' ORDER BY supplier.NAME, SUPPLIER_PRODUCT_CODE ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {
                    return json_encode(array("response" => true, "removed_products" => $datos));
                } else {
                    return json_encode(array("response" => true, "removed_products" => array()));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/removed_product/{supplier_code}/{supplier_product_code}', function ($request, $response, array $args) {
    $args['supplier_product_code'] = convert_data_url($args['supplier_product_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_product_code']);
    $sentencia = $this->db->prepare("SELECT supplier_product_tmp.*, supplier.NAME as SUPPLIER_NAME FROM supplier_product_tmp, supplier WHERE supplier.SUPPLIER_CODE = supplier_product_tmp.SUPPLIER_CODE AND supplier_product_tmp.SUPPLIER_CODE = ? AND supplier_product_tmp.SUPPLIER_PRODUCT_CODE = ?");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "removed_product" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {
                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/removed_product/{supplier_code}/{supplier_product_code}', function ($request, $response, array $args) {
    $args['supplier_product_code'] = convert_data_url($args['supplier_product_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $data = $request->getParsedBody();
    $cicerone_product_code = ((filter_var($data['cicerone_product_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['cicerone_product_code'], FILTER_SANITIZE_STRING));
    $supplier_product_code_new = ((filter_var($data['supplier_product_code_new'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['supplier_product_code_new'], FILTER_SANITIZE_STRING));

    if ($cicerone_product_code == "_NEW_PRODUCT_") {
        $cicerone_product_code = $supplier_product_code_new;
    }

    $subsegment_code = ((filter_var($data['subsegment_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['subsegment_code'], FILTER_SANITIZE_STRING));
    $morning_period_code = ((filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['morning_period_code'], FILTER_SANITIZE_STRING));
    $afternoon_period_code = ((filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['afternoon_period_code'], FILTER_SANITIZE_STRING));
    $night_period_code = ((filter_var($data['night_period_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['night_period_code'], FILTER_SANITIZE_STRING));
    $city_code = ((filter_var($data['city_code'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['city_code'], FILTER_SANITIZE_STRING));
    $description = ((filter_var($data['description'], FILTER_SANITIZE_STRING) == "") ? NULL : filter_var($data['description'], FILTER_SANITIZE_STRING));
    $arrayValores = array($cicerone_product_code, $subsegment_code, $morning_period_code, $afternoon_period_code, $night_period_code, $city_code, $description, $args['supplier_code'], $args['supplier_product_code']);
    $sentencia = $this->db->prepare("UPDATE supplier_product_tmp SET CICERONE_PRODUCT_CODE = ?, SUBSEGMENT_CODE = ?, MORNING_PERIOD_CODE = ?, AFTERNOON_PERIOD_CODE = ?, NIGHT_PERIOD_CODE = ?, CITY_CODE = ?, DESCRIPTION = ? WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");

    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/removed_product/{supplier_code}/{supplier_product_code}', function ($request, $response, array $args) {
    $args['supplier_product_code'] = convert_data_url($args['supplier_product_code']);
    $args['supplier_code'] = convert_data_url($args['supplier_code']);
    $arrayValores = array($args['supplier_code'], $args['supplier_product_code']);
    $sentencia = $this->db->prepare("UPDATE supplier_product_tmp SET VISIBLE = 'Y' WHERE SUPPLIER_CODE = ? AND SUPPLIER_PRODUCT_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});