<?php
$app->get('/periods', function ($request, $response, array $args) {
    $arrayValores = array();
    $sentencia = $this->db->prepare("SELECT * FROM period ORDER BY P_ORDER ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "periods" => $datos));
                } else {

                    return json_encode(array("response" => true, "periods" => array()));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/periods/{type}', function ($request, $response, array $args) {
    $args['type'] = convert_data_url($args['type']);
    $arrayValores = array($args['type'] . "%");
    $sentencia = $this->db->prepare("SELECT * FROM period WHERE PERIOD_CODE LIKE ? ORDER BY P_ORDER ASC");
    if (!$sentencia) {
        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "periods" => $datos));
                } else {

                    return json_encode(array("response" => true, "periods" => array()));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->get('/period/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("SELECT * FROM period WHERE PERIOD_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {
                $datos = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                if (count($datos) > 0) {

                    return json_encode(array("response" => true, "period" => $datos[0]));
                } else {


                    return $response->withStatus(401)
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode(array("response" => false, "error" => "SIN RESULTADOS")));
                }
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/period', function ($request, $response, array $args) {
    $data = $request->getParsedBody();
    $code = filter_var($data['code'], FILTER_SANITIZE_STRING);
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $p_order = filter_var($data['p_order'], FILTER_SANITIZE_STRING);
    if (isset($data["start_hour"])) {
        $start_hour = filter_var($data['start_hour'], FILTER_SANITIZE_STRING);
    } else {
        $start_hour = null;
    }
    if (isset($data["end_hour"])) {
        $end_hour = filter_var($data['end_hour'], FILTER_SANITIZE_STRING);
    } else {
        $end_hour = null;
    }
    $arrayValores = array($code, $description, $start_hour, $end_hour, $p_order);
    $sentencia = $this->db->prepare("INSERT INTO period (PERIOD_CODE, DESCRIPTION, START_HOUR, END_HOUR, P_ORDER) VALUES (?,?,?,?,?)");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->post('/period/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $data = $request->getParsedBody();
    $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $p_order = filter_var($data['p_order'], FILTER_SANITIZE_STRING);
    if (isset($data["start_hour"])) {
        $start_hour = filter_var($data['start_hour'], FILTER_SANITIZE_STRING);
    } else {
        $start_hour = null;
    }
    if (isset($data["end_hour"])) {
        $end_hour = filter_var($data['end_hour'], FILTER_SANITIZE_STRING);
    } else {
        $end_hour = null;
    }
    $arrayValores = array($description, $start_hour, $end_hour, $p_order, $args['code']);
    $sentencia = $this->db->prepare("UPDATE period SET DESCRIPTION = ?, START_HOUR = ?, END_HOUR = ?, P_ORDER = ? WHERE PERIOD_CODE = ?");

    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_GUARDADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});
$app->delete('/period/{code}', function ($request, $response, array $args) {
    $args['code'] = convert_data_url($args['code']);
    $arrayValores = array($args['code']);
    $sentencia = $this->db->prepare("DELETE FROM period WHERE PERIOD_CODE = ?");
    if (!$sentencia) {


        return $response->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
    } else {
        try {
            if ($sentencia->execute($arrayValores)) {

                return json_encode(array("response" => true, "message" => "t(REGISTRO_ELIMINADO)"));
            } else {


                return $response->withStatus(401)
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode(array("response" => false, "error" => $this->db->errorInfo())));
            }
        } catch (Exception $e) {

            return $response->withStatus(401)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(array("response" => false, "error" => $e->getMessage())));
        }
    }
});