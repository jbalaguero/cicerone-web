<?php
date_default_timezone_set("Europe/Madrid");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['db']['host'] = getenv('DB_HOST');
$config['db']['user'] = getenv('DB_USER');
$config['db']['pass'] = getenv('DB_PASSWORD');
$config['db']['dbname'] = getenv('DB_NAME');

$app = new \Slim\App(['settings' => $config]);

$container = $app->getContainer();
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'] . ";charset=utf8",
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};
$container['beginTransaction'] = function ($c) {
    $c['db']->beginTransaction();
};


include "include/segment.php";
include "include/subsegment.php";
include "include/currency.php";
include "include/country.php";
include "include/agent.php";
include "include/platform.php";
include "include/supplier.php";
include "include/city.php";
include "include/period.php";
include "include/product.php";
include "include/pending_product.php";
include "include/removed_product.php";
include "include/cicerone.php";
include "include/pending_agents.php";
include "include/removed_agent.php";
$app->get('/hello/{name}', function ($request, $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");
    return $response;
});
$app->run();

function convert_data_url($message)
{
    return str_ireplace("___SPACE___", " ", str_ireplace("___PLUS___", "+", str_ireplace("___VACIO___", "", $message)));
}
