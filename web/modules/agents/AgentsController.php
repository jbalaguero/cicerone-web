<?php

namespace Cicerone;

class AgentsController extends Controller
{
    protected $myTools;

    public function load()
    {
        $this->nombre_modulo = "Agents";
        $this->myTools = new Tools();

        if ($this->getUser()->checkPermisoText("MODULO_AGENTS")) {
            call_user_func_array(array($this, ACCION_BACK . "Action"), func_get_args());
        } else {
            header("Location: " . ROOTPATH . "dashboard");
            die();
        }
    }

    public function listAction()
    {
        $this->js = "<script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/list.js'></script>";
        $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php");
    }

    public function newAction()
    {
        $array_elemento_vacio = array(
            "AGENT_CODE" => "",
            "NAME" => ""
        );
        $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
        $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/edit.php", array(
            "elemento" => $array_elemento_vacio,
            "url_form" => API_URL . "/agent"
        ));
    }

    public function editAction()
    {
        $respuesta = $this->myTools->peticionCURL("GET", "/agent/" . ELEMENTO_BACK, array());

        if ($respuesta["response"]) {
            $elemento = $respuesta["agent"];
            $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
            $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php", array(
                "elemento" => $elemento,
                "url_form" => API_URL . "/agent/" . $elemento["AGENT_CODE"]
            ));
        } else {
            die("Elemento no encontrado");
        }
    }
}