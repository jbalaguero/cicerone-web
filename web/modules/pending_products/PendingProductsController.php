<?php

namespace Cicerone;

class PendingProductsController extends Controller
{
    protected $myTools;

    public function load()
    {
        $this->nombre_modulo = "Pending products";
        $this->myTools = new Tools();

        if ($this->getUser()->checkPermisoText("MODULO_PENDINGPRODUCTS")) {
            call_user_func_array(array($this, ACCION_BACK . "Action"), func_get_args());
        } else {
            header("Location: " . ROOTPATH . "dashboard");
            die();
        }
    }

    public function listAction()
    {
        $this->js = "<script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/list.js'></script>";
        $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php");
    }

    public function editAction()
    {
        $datosURL=explode(";", ELEMENTO_BACK);
        if($datosURL[0]==""){
            $datosURL[0]="___VACIO___";
        }
        if($datosURL[1]==""){
            $datosURL[1]="___VACIO___";
        }
        $respuesta = $this->myTools->peticionCURL("GET", "/pending_product/" . implode("/", $datosURL), array());

        if ($respuesta["response"]) {
            $elemento = $respuesta["pending_product"];
            $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
            $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php", array(
                "elemento" => $elemento,
                "url_form" => API_URL . "/pending_product/" . ($elemento["SUPPLIER_CODE"] != "" ? $elemento["SUPPLIER_CODE"] : "___VACIO___") . "/" . ($elemento["SUPPLIER_PRODUCT_CODE"] != "" ? $elemento["SUPPLIER_PRODUCT_CODE"] : "___VACIO___")
            ));
        } else {
            die("Elemento no encontrado");
        }
    }
}