var aux_tipo_save = null;
$(document).ready(function () {
    $("#formFormulario").validate({
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
                $(placement).addClass("conError");
            } else {
                error.insertAfter(element);
            }
        },
        rules:{
            morning_period_code:{
                required: function(){
                    return (($("#formAfternoonPeriodCode").val() == "") && ($("#formNightPeriodCode").val() == ""));
                }
            },
            afternoon_period_code:{
                required: function(){
                    return (($("#formMorgingPeriodCode").val() == "") && ($("#formNightPeriodCode").val() == ""));
                }
            },
            night_period_code:{
                required: function(){
                    return (($("#formAfternoonPeriodCode").val() == "") && ($("#formMorgingPeriodCode").val() == ""));
                }
            },
            supplier_product_code_new:{
                required: function(){
                    return (($("#formCiceroneProductCode").val() == "_NEW_PRODUCT_"));
                }
            }
        },
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        }
    });

    $("#formFormulario").ajaxForm({
        dataType: 'json',
        beforeSubmit: function (data, form) {
            if ($(form).valid()) {
                KTApp.blockPage({overlayColor: "#000000", state: "primary"});
                return true;
            } else {
                return false;
            }
        },
        success: function (responseJSON, type, response, form) {
            KTApp.unblockPage()
            show_message_alert(responseJSON.message, 'success');
            if (aux_tipo_save == "GUARDAR_Y_SALIR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/list/list";
            } else if (aux_tipo_save == "GUARDAR_Y_CONTINUAR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/edit/" + ELEMENTO_BACK;
            }
        },
        error: function (data) {
            KTApp.unblockPage()
            show_message_alert(data.responseJSON.error, 'danger');
        }
    });

    $("#formCiceroneProductCode").change(function(){
        if($(this).val()=="_NEW_PRODUCT_"){
            $("#formSubsegmentCode").val("");
            $("#formSubsegmentCode").closest(".form-group").removeClass("readOnlyField");
            $("#formMorgingPeriodCode").val("");
            $("#formMorgingPeriodCode").closest(".form-group").removeClass("readOnlyField");
            $("#formAfternoonPeriodCode").val("");
            $("#formAfternoonPeriodCode").closest(".form-group").removeClass("readOnlyField");
            $("#formNightPeriodCode").val("");
            $("#formNightPeriodCode").closest(".form-group").removeClass("readOnlyField");
            $("#formCityCode").val("");
            $("#formCityCode").closest(".form-group").removeClass("readOnlyField");
            $("#formNewCode").slideDown()
        }else{
            KTApp.blockPage();
            $.ajax({
                dataType: 'json',
                method: "GET",
                url: API_URL + "/product/"+$(this).val()
            }).done(function (response) {
                KTApp.unblockPage()
                $("#formSubsegmentCode").val(response.product.SUBSEGMENT_CODE);
                $("#formSubsegmentCode").closest(".form-group").addClass("readOnlyField");
                $("#formMorgingPeriodCode").val(response.product.MORNING_PERIOD_CODE);
                $("#formMorgingPeriodCode").closest(".form-group").addClass("readOnlyField");
                $("#formAfternoonPeriodCode").val(response.product.AFTERNOON_PERIOD_CODE);
                $("#formAfternoonPeriodCode").closest(".form-group").addClass("readOnlyField");
                $("#formNightPeriodCode").val(response.product.NIGHT_PERIOD_CODE);
                $("#formNightPeriodCode").closest(".form-group").addClass("readOnlyField");
                $("#formCityCode").val(response.product.CITY_CODE);
                $("#formCityCode").closest(".form-group").addClass("readOnlyField");
                $("#formNewCode").slideUp()
            }).fail(function (response) {
                $(this).val("_NEW_PRODUCT_");
                $("#formCiceroneProductCode").change();
                KTApp.unblockPage();

            });
        }
    });
    $("#formCiceroneProductCode").change();
});
function save_formulario(tipo) {
    aux_tipo_save = tipo;
    $("#formFormulario").submit();
}