var table_dt = null;
var oTable_dt = null;

jQuery(document).ready(function () {
    table_dt = $('.datatable_script');
    // begin first table

    option_this_datatables = option_comunes_datatables;
    option_this_datatables["ordering"] = false;
    option_this_datatables["select"] = {
        style: 'multi',
        selector: '.checkbox_datatable'
    };
    option_this_datatables["order"] = [[0, "asc"], [1, "asc"]];
    option_this_datatables["ajax"] = {
        "type": "GET",
        "url": table_dt.attr("data-url"),
        "dataSrc": function (json) {
            return json.pending_products;
        }
    };
    option_this_datatables["columns"] = [
        {'data': 'SUPPLIER_NAME'},
        {'data': 'SUPPLIER_PRODUCT_CODE'},
        {'data': 'CICERONE_PRODUCT_CODE'},
        {'data': 'DESCRIPTION'},
        {'data': 'SUPPLIER_CITY_NAME'},
        {'data': 'SUPPLIER_CODE_SUPPLIER_PRODUCT_CODE'}
    ];
    option_this_datatables["columnDefs"] = [
        {
            targets: 0,
            render: function (data, type, full, meta) {

                datos = data.split("#");

                output = `<span class="checkbox_datatable" ><i class="flaticon2-check-mark" ></i></span><a style="color: black;font-weight: 500" href="` + ROOTPATH + `` + MODULO_BACK + `/edit/` + datos[1] + `" class="kt-user-card-v2__email kt-link"><span class="kt-user-card-v2__name">` + datos[0] + `</span></a>`;

                return output;
            },
        },
        {
            targets: -1,
            orderable: false,
            render: function (data, type, full, meta) {
                return `
                        <a href="` + ROOTPATH + `` + MODULO_BACK + `/edit/` + data + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("EDITAR_REGISTRO") + `">
                          <i class="la la-pencil"></i>
                        </a>
                        <a href="javascript:delete_element('` + data + `')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("ELIMINAR_REGISTRO") + `">
                          <i class="la la-trash"></i>
                        </a>`;
            },
        }
    ];
    oTable_dt = table_dt.DataTable(option_this_datatables);
    var buttons = new $.fn.dataTable.Buttons(oTable_dt, {
        buttons: button_array_buttons
    }).container().prependTo($('.kt-subheader__toolbar'));

    $("#btnMove").click(function () {
        codes_to_move = [];
        oTable_dt.rows({selected: true}).every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            codes_to_move.push(data["SUPPLIER_NAME"].split("#")[1]);
        });
        if (codes_to_move.length > 0) {
            swal.fire({
                title: t('ATENCION'),
                text: t('¿QUIERES_MOVER_ESTOS_PRODUCTOS_A_CICERONE?'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: t('ACEPTAR'),
                cancelButtonText: t('CANCEL'),
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    KTApp.blockPage({overlayColor: "#000000", state: "primary"});
                    $.ajax({
                        dataType: 'json',
                        method: "POST",
                        url: API_URL + "/pending_product/move_products",
                        data: {
                            codes_to_move: codes_to_move
                        }
                    }).done(function (response) {
                        KTApp.unblockPage()
                        show_message_alert(t('PROCESO_FINALIZADO,ELEMENTOS_PROCESADOS') + response.message, response.type);
                        oTable_dt.ajax.reload(null, false);
                    }).fail(function (response) {
                        KTApp.unblockPage()
                        show_message_alert(response.responseJSON.error, "danger");
                        oTable_dt.ajax.reload(null, false);
                    });
                } else if (result.dismiss === 'cancel') {

                }
            });
        } else {
            show_message_alert(t('DEBES_SELECCIONAR_ALGUN_ELEMENTO'), "danger")
        }
    });
});

function delete_element(id) {
    swal.fire({
        title: t('¿ESTAS_SEGURO?'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: t('SI_ELIMINAR'),
        cancelButtonText: t('NO_CANCELAR'),
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            KTApp.blockPage({overlayColor: "#000000", state: "primary"});


            var datos_url=id.split(";");
            if(datos_url[0]==""){
                datos_url[0]="___VACIO___"
            }
            if(datos_url[1]==""){
                datos_url[1]="___VACIO___"
            }

            $.ajax({
                dataType: 'json',
                method: "DELETE",
                url: API_URL + "/pending_product/" + datos_url.join("/")
            }).done(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.message, "success");
                oTable_dt.ajax.reload(null, false);
            }).fail(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.responseJSON.error, "danger");
                oTable_dt.ajax.reload(null, false);
            });
        } else if (result.dismiss === 'cancel') {

        }
    });
}