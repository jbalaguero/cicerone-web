var table_dt = null;
var oTable_dt = null;
var aux_tipo_save = null;
var permitir_add = false;
$(document).ready(function () {
    $("#formFormulario").validate({
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
                $(placement).addClass("conError");
            } else {
                error.insertAfter(element);
            }
        },
        rules:{
            morning_period_code:{
                required: function(){
                    return (($("#formAfternoonPeriodCode").val() == "") && ($("#formNightPeriodCode").val() == ""));
                }
            },
            afternoon_period_code:{
                required: function(){
                    return (($("#formMorgingPeriodCode").val() == "") && ($("#formNightPeriodCode").val() == ""));
                }
            },
            night_period_code:{
                required: function(){
                    return (($("#formAfternoonPeriodCode").val() == "") && ($("#formMorgingPeriodCode").val() == ""));
                }
            }
        },
        onfocusout: false,
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        }
    });

    $("#formFormulario").ajaxForm({
        dataType: 'json',
        beforeSubmit: function (data, form) {
            if ($(form).valid()) {
                KTApp.blockPage({overlayColor:"#000000",state:"primary"});
                return true;
            } else {
                return false;
            }
        },
        success: function (responseJSON, type, response, form) {
            KTApp.unblockPage()
            show_message_alert(responseJSON.message, 'success');
            if (aux_tipo_save == "GUARDAR_Y_SALIR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/list/list";
            } else if (aux_tipo_save == "GUARDAR_Y_CREAR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/new/element";
            } else if (aux_tipo_save == "GUARDAR_Y_CONTINUAR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/edit/"+$("#formCode").val();
            }
        },
        error: function (data) {
            KTApp.unblockPage()
            show_message_alert(data.responseJSON.error, 'danger');
        }
    });

    table_dt = $('.datatable_script');
    // begin first table

    option_this_datatables = option_comunes_datatables;
    option_this_datatables["order"] = [[1, "asc"]];
    option_this_datatables["ajax"] = {
        "type": "GET",
        "url": table_dt.attr("data-url"),
        "dataSrc": function (json) {
            return json.mappings;
        }
    };
    option_this_datatables["columns"] = [
        {'data': 'SUPPLIER_NAME'},
        {'data': 'SUPPLIER_PRODUCT_CODE'},
        {'data': 'SUPPLIER_CODE_SUPPLIER_PRODUCT_CODE'}
    ];
    option_this_datatables["columnDefs"] = [
        {
            targets: -1,
            orderable: false,
            render: function (data, type, full, meta) {
                return `
                        <a href="javascript:delete_mapping('` + ELEMENTO_BACK + `', '` + data.split("-")[0] + `', '` + data.split("-")[1] + `')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("ELIMINAR_REGISTRO") + `">
                          <i class="la la-trash"></i>
                        </a>`;
            },
        }
    ];
    option_this_datatables["ordering"] = false;
    option_this_datatables["paging"] = false;
    option_this_datatables["searching"] = false;
    option_this_datatables["info"] = false;
    oTable_dt = table_dt.DataTable(option_this_datatables);

    get_suppiers();
});

function get_suppiers() {
    $.ajax({
        dataType: 'json',
        method: "GET",
        url: API_URL + "/product/" + ELEMENTO_BACK + "/suppliers_allowed"
    }).done(function (response) {
        $("#formSupplierCode").empty();
        if (response.suppliers.length > 0) {
            response.suppliers.forEach(function (element) {
                $("#formSupplierCode").append("<option value='" + element.SUPPLIER_CODE + "'>" + element.NAME + "</option>");
            });
            $("#btnAddMapping").removeAttr("disabled");
            $("#btnAddMapping").removeClass("disabled");
            permitir_add = true;
        } else {
            $("#btnAddMapping").attr("disabled", "disabled");
            $("#btnAddMapping").addClass("disabled");
            permitir_add = false;
        }
    }).fail(function (response) {
        show_message_alert(response.responseJSON.error, "danger");
        oTable_dt.ajax.reload(null, false);
    });

    $("#formAddMapping").submit(function(){
        return false;
    });
}

function save_formulario(tipo) {
    aux_tipo_save = tipo;
    $("#formFormulario").submit();
}

function add_mapping() {
    if(permitir_add && $("#formSupplierProductCode").valid()){
        $.ajax({
            dataType: 'json',
            method: "POST",
            url: API_URL + "/product/" + ELEMENTO_BACK + "/supplier/" + $("#formSupplierCode").val(),
            data: {
                supplier_product_code: $("#formSupplierProductCode").val()
            }
        }).done(function (response) {
            KTApp.unblockPage()
            show_message_alert(response.message, "success");
            oTable_dt.ajax.reload(null, false);
            get_suppiers();
            $("#formAddMapping")[0].reset();
        }).fail(function (response) {
            KTApp.unblockPage()
            show_message_alert(response.responseJSON.error, "danger");
            oTable_dt.ajax.reload(null, false);
            get_suppiers();
            $("#formAddMapping")[0].reset();
        });
    }
}

function delete_mapping(product, supplier, supplier_product_code) {
    swal.fire({
        title: t('¿ESTAS_SEGURO?'),
        text: t('NO_SE_PUEDE_DESAHACER'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: t('SI_ELIMINAR'),
        cancelButtonText: t('NO_CANCELAR'),
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            KTApp.blockPage({overlayColor: "#000000", state: "primary"});
            $.ajax({
                dataType: 'json',
                method: "DELETE",
                url: API_URL + "/product/" + product + "/supplier/" + supplier,
                data: {
                    supplier_product_code: supplier_product_code
                }
            }).done(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.message, "success");
                oTable_dt.ajax.reload(null, false);
                get_suppiers();
            }).fail(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.responseJSON.error, "danger");
                oTable_dt.ajax.reload(null, false);
                get_suppiers();
            });
        } else if (result.dismiss === 'cancel') {

        }
    });
}