
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    <?= $this->nombre_modulo ?>
                </h3>
            </div>
            <div class="kt-subheader__toolbar">
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-portlet kt-portlet--mobile">

            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable datatable_script" id="datatable-ajax-<?= MODULO_BACK ?>-<?= ACCION_BACK ?>" data-url="<?= API_URL ?>/removed_agents">
                    <thead>
                    <tr>
                        <th style="min-width: 200px"><?=$this->t("SUPPLIER")?></th>
                        <th><?=$this->t("SUPPLIER_AGENT_CODE")?></th>
                        <th><?=$this->t("CICERONE_AGENT_CODE")?></th>
                        <th style="max-width: 300px"><?=$this->t("SUPPLIER_AGENT_NAME")?></th>
                        <th style="min-width: 100px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->

</div>