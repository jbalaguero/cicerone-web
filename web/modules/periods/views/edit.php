<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    <?= $this->nombre_modulo ?>
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total"><?= $data["elemento"]["PERIOD_CODE"] ?></span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <a href="<?= ROOTPATH ?><?= MODULO_BACK ?>/list/list" class="btn btn-default btn-bold"><?= $this->t("ATRAS") ?></a>
                <div class="btn-group">
                    <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="btn btn-brand btn-bold">
                        <?= $this->t("GUARDAR") ?>
                    </a>
                    <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_CONTINUAR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_CONTINUAR") ?></span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_CREAR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_CREAR") ?></span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_SALIR") ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__body">
                <form id="formFormulario" method="post" action="<?= $data["url_form"] ?>">
                    <div class="kt-form kt-form--label-right">
                        <div class="kt-form__body">
                            <div class="row">
                                <div class="col-3">
                                    <?= $this->fieldGrid("text", $this->t("CODE"), array(
                                        "id" => "formCode",
                                        "name" => "code",
                                        "value" => $data["elemento"]["PERIOD_CODE"],
                                        "maxlength" => "2",
                                        (ACCION_BACK=="edit" ? "disabled " : "")."required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-7">
                                    <?= $this->fieldGrid("text", $this->t("DESCRIPTION"), array(
                                        "id" => "formDescription",
                                        "name" => "description",
                                        "value" => $data["elemento"]["DESCRIPTION"],
                                        "maxlength" => "50",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-2">
                                    <?= $this->fieldGrid("number", $this->t("P_ORDER"), array(
                                        "id" => "formPOrder",
                                        "name" => "p_order",
                                        "value" => $data["elemento"]["P_ORDER"],
                                        "maxlength" => "11",
                                        "required" => "required"
                                    )) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <?= $this->fieldGrid("text", $this->t("START_HOUR"), array(
                                        "id" => "formStartHour",
                                        "name" => "start_hour",
                                        (in_array($data["elemento"]["PERIOD_CODE"], array("M", "A")) ? "disabled " : "required ")."value" => (in_array($data["elemento"]["PERIOD_CODE"], array("M", "A")) ? "" : $data["elemento"]["START_HOUR"])
                                    )) ?>
                                </div>
                                <div class="col-3">
                                    <?= $this->fieldGrid("text", $this->t("END_HOUR"), array(
                                        "id" => "formEndHour",
                                        "name" => "end_hour",
                                        (in_array($data["elemento"]["PERIOD_CODE"], array("M", "A")) ? "disabled " : "required ")."value" => (in_array($data["elemento"]["PERIOD_CODE"], array("M", "A")) ? "" : $data["elemento"]["END_HOUR"])
                                    )) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>