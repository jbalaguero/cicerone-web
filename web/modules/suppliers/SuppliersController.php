<?php

namespace Cicerone;

class SuppliersController extends Controller
{
    protected $myTools;

    public function load()
    {
        $this->nombre_modulo = "Suppliers";
        $this->myTools = new Tools();

        if ($this->getUser()->checkPermisoText("MODULO_SUPPLIERS")) {
            call_user_func_array(array($this, ACCION_BACK . "Action"), func_get_args());
        } else {
            header("Location: " . ROOTPATH . "dashboard");
            die();
        }
    }

    public function listAction()
    {
        $this->js = "<script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/list.js'></script>";
        $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php");
    }

    public function newAction()
    {
        $array_elemento_vacio = array(
            "SUPPLIER_CODE" => "",
            "NAME" => "",
            "DATAMETRYCS_NAME" => "",
            "BI_CLIENT_ID" => "",
            "BI_CLIENT_SECRET" => ""
        );
        $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
        $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/edit.php", array(
            "elemento" => $array_elemento_vacio,
            "url_form" => API_URL . "/supplier"
        ));
    }

    public function editAction()
    {
        $respuesta = $this->myTools->peticionCURL("GET", "/supplier/" . ELEMENTO_BACK, array());

        if ($respuesta["response"]) {
            $elemento = $respuesta["supplier"];
            $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
            $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php", array(
                "elemento" => $elemento,
                "url_form" => API_URL . "/supplier/" . $elemento["SUPPLIER_CODE"]
            ));
        } else {
            die("Elemento no encontrado");
        }
    }
}