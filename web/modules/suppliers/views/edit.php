<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    <?= $this->nombre_modulo ?>
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total"><?= $data["elemento"]["SUPPLIER_CODE"] ?></span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <a href="<?= ROOTPATH ?><?= MODULO_BACK ?>/list/list" class="btn btn-default btn-bold"><?= $this->t("ATRAS") ?></a>
                <div class="btn-group">
                    <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="btn btn-brand btn-bold">
                        <?= $this->t("GUARDAR") ?>
                    </a>
                    <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_CONTINUAR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_CONTINUAR") ?></span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_CREAR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_CREAR") ?></span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_SALIR") ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__body">
                <form id="formFormulario" method="post" action="<?= $data["url_form"] ?>">
                    <div class="kt-form kt-form--label-right">
                        <div class="kt-form__body">
                            <div class="row">
                                <div class="col-2">
                                    <?= $this->fieldGrid("text", $this->t("CODE"), array(
                                        "id" => "formCode",
                                        "name" => "code",
                                        "value" => $data["elemento"]["SUPPLIER_CODE"],
                                        "maxlength" => "10",
                                        (ACCION_BACK=="edit" ? "disabled " : "")."required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-5">
                                    <?= $this->fieldGrid("text", $this->t("NAME"), array(
                                        "id" => "formName",
                                        "name" => "name",
                                        "value" => $data["elemento"]["NAME"],
                                        "maxlength" => "30",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-5">
                                    <?= $this->fieldGrid("text", $this->t("DATAMETRYCS_NAME"), array(
                                        "id" => "formDatametrycsName",
                                        "name" => "datametrycs_name",
                                        "value" => $data["elemento"]["DATAMETRYCS_NAME"],
                                        "maxlength" => "30",
                                        "required" => "required"
                                    )) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <?= $this->fieldGrid("text", $this->t("BI_CLIENT_ID"), array(
                                        "id" => "formBiClientId",
                                        "name" => "bi_client_id",
                                        "value" => $data["elemento"]["BI_CLIENT_ID"],
                                        "maxlength" => "128"
                                    )) ?>
                                </div>
                                <div class="col-3">
                                    <?= $this->fieldGrid("text", $this->t("BI_CLIENT_SECRET"), array(
                                        "id" => "formBiClientSecret",
                                        "name" => "bi_client_secret",
                                        "value" => $data["elemento"]["BI_CLIENT_SECRET"],
                                        "maxlength" => "128"
                                    )) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if(ACCION_BACK == "edit"){
                    ?>
                    <div class="kt-divider">
                        <span></span>
                        <span></span>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <h3><?=$this->t("PLATFORMS")?></h3>
                        </div>
                    </div>
                    <form id="formAddPlatform">
                        <div class="row">
                            <div class="col-1">
                                <?= $this->fieldGrid("hidden", "", array(
                                    "id" => "formEditMode",
                                    "name" => "edit_mode",
                                    "value" => "0"
                                )) ?>
                                <?= $this->fieldGrid("hidden", "", array(
                                    "id" => "formLastPlatformCode",
                                    "name" => "last_platform_code"
                                )) ?>
                                <?= $this->fieldGrid("select", $this->t("PLATFORM"), array(
                                    "id" => "formPlatformCode",
                                    "name" => "platform_code"
                                )) ?>
                            </div>
                            <div class="col-1">
                                <?= $this->fieldGrid("select", $this->t("CITY_CODE"), array(
                                    "id" => "formCityCode",
                                    "name" => "city_code",
                                    "options" => array("" => strip_tags($this->t("SELECT_CITY"))),
                                    "options_url" => array(
                                        "url" => API_URL."/cities",
                                        "index" => "cities",
                                        "value" => "CITY_CODE",
                                        "label" => "DESCRIPTION"
                                    )
                                )) ?>
                            </div>
                            <div class="col-2 campos_api" style="display: none">
                                <?= $this->fieldGrid("text", $this->t("CLIENT_ID"), array(
                                    "id" => "formClientId",
                                    "name" => "client_id"
                                )) ?>
                            </div>
                            <div class="col-2 campos_api" style="display: none">
                                <?= $this->fieldGrid("text", $this->t("CLIENT_SECRET"), array(
                                    "id" => "formClientSecret",
                                    "name" => "client_secret"
                                )) ?>
                            </div>
                            <div class="col-2 campos_api" style="display: none">
                                <?= $this->fieldGrid("text", $this->t("API_KEY"), array(
                                    "id" => "formApiKey",
                                    "name" => "api_key",
                                    "maxlength" => "128"
                                )) ?>
                            </div>
                            <div class="col-2 campos_api" style="display: none">
                                <?= $this->fieldGrid("text", $this->t("LAST_DAY_PROCESSED"), array(
                                    "id" => "formLastDayProcessed",
                                    "name" => "last_day_processed",
                                    "required" => "required",
                                    "class" => array("datepicker_field"),
                                    "readonly" => "readonly"
                                )) ?>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label class="col-form-label">&nbsp;</label>
                                    <div>
                                        <a id="btnAddPlatform" href="javascript:add_platform()" class="btn btn-brand btn-bold disabled"><?= $this->t("ANADIR_PLATFORM") ?></a>
                                        <a id="btnCloseEditPlatform" href="javascript:close_edit_platform()" class="btn btn-default btn-bold" style="display: none"><?= $this->t("CERRAR") ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable datatable_script" id="datatable-ajax-<?= MODULO_BACK ?>-<?= ACCION_BACK ?>" data-url="<?= API_URL ?>/supplier/<?= ELEMENTO_BACK ?>/platforms">
                        <thead>
                        <tr>
                            <th><?= $this->t("PLATFORM") ?></th>
                            <th><?= $this->t("CITY") ?></th>
                            <th><?= $this->t("CLIENT_ID") ?></th>
                            <th><?= $this->t("CLIENT_SECRET") ?></th>
                            <th><?= $this->t("API_KEY") ?></th>
                            <th><?= $this->t("LAST_DAY_PROCESSED") ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>