var table_dt = null;
var oTable_dt = null;
var aux_tipo_save = null;
var permitir_add = false;
$(document).ready(function () {
    $("#formFormulario").validate({
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
                $(placement).addClass("conError");
            } else {
                error.insertAfter(element);
            }
        },
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            bi_client_id: {
                required: function () {
                    if ($("#formBiClientSecret").val().split(" ").join("") != "") {
                        return true;
                    }
                    return false;
                }
            },
            bi_client_secret: {
                required: function () {
                    if ($("#formBiClientId").val().split(" ").join("") != "") {
                        return true;
                    }
                    return false;
                }
            }
        }
    });

    $("#formFormulario").ajaxForm({
        dataType: 'json',
        beforeSubmit: function (data, form) {
            if ($(form).valid()) {
                KTApp.blockPage({overlayColor: "#000000", state: "primary"});
                return true;
            } else {
                return false;
            }
        },
        success: function (responseJSON, type, response, form) {
            KTApp.unblockPage()
            show_message_alert(responseJSON.message, 'success');
            if (aux_tipo_save == "GUARDAR_Y_SALIR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/list/list";
            } else if (aux_tipo_save == "GUARDAR_Y_CREAR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/new/element";
            } else if (aux_tipo_save == "GUARDAR_Y_CONTINUAR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/edit/" + $("#formCode").val();
            }
        },
        error: function (data) {
            KTApp.unblockPage()
            show_message_alert(data.responseJSON.error, 'danger');
        }
    });

    table_dt = $('.datatable_script');
    // begin first table

    option_this_datatables = option_comunes_datatables;
    option_this_datatables["order"] = [[1, "asc"]];
    option_this_datatables["ajax"] = {
        "type": "GET",
        "url": table_dt.attr("data-url"),
        "dataSrc": function (json) {
            return json.mappings;
        }
    };
    option_this_datatables["columns"] = [
        {'data': 'PLATFORM_NAME'},
        {'data': 'CITY_NAME'},
        {'data': 'CLIENT_ID'},
        {'data': 'CLIENT_SECRET'},
        {'data': 'API_KEY'},
        {'data': 'LAST_DAY_PROCESSED'},
        {'data': 'PLATFORM_CODE'}
    ];
    option_this_datatables["columnDefs"] = [
        {
            targets: -1,
            orderable: false,
            render: function (data, type, full, meta) {
                if(full['API_KEY']==null){
                    full['API_KEY']='';
                }
                if(full['CLIENT_ID']==null){
                    full['CLIENT_ID']='';
                }
                if(full['CLIENT_SECRET']==null){
                    full['CLIENT_SECRET']='';
                }
                if(full['LAST_DAY_PROCESSED']==null){
                    full['LAST_DAY_PROCESSED']='';
                }
                return `
                        <a href="javascript:edit_platform('` + ELEMENTO_BACK + `', '` + full['API_KEY'] + `', '` + full['CITY_CODE'] + `', '` + full['CITY_NAME'] + `', '` + full['CLIENT_ID'] + `', '` + full['CLIENT_SECRET'] + `', '` + full['LAST_DAY_PROCESSED'] + `', '` + full['PLATFORM_CODE'] + `', '` + full['PLATFORM_NAME'] + `')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("EDITAR_REGISTRO") + `">
                          <i class="la la-pencil"></i>
                        </a>
                        <a href="javascript:delete_platform('` + ELEMENTO_BACK + `', '` + data + `')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("ELIMINAR_REGISTRO") + `">
                          <i class="la la-trash"></i>
                        </a>`;
            },
        }
    ];
    option_this_datatables["ordering"] = false;
    option_this_datatables["paging"] = false;
    option_this_datatables["searching"] = false;
    option_this_datatables["info"] = false;
    oTable_dt = table_dt.DataTable(option_this_datatables);

    get_platforms(false);

    $("#btnAddPlatform").submit(function () {
        return false;
    });

    $("#formPlatformCode").change(function () {
        get_platform($("#formPlatformCode").val())
    });
    $("#formPlatformCode").change();
});

function get_platform(code) {
    $.ajax({
        dataType: 'json',
        method: "GET",
        url: API_URL + "/platform/" + code
    }).done(function (response) {
        if(response.platform.PLATFORM_TYPE=="A"){
            $(".campos_api").slideDown();
            $("#formAddPlatform").addClass("api_mode");
        }else{
            $(".campos_api").slideUp();
            $("#formAddPlatform").removeClass("api_mode");
        }
    }).fail(function (response) {
        /*show_message_alert(response.responseJSON.error, "danger");
        oTable_dt.ajax.reload(null, false);*/
    });
}

function get_platforms(for_edit) {
    $.ajax({
        dataType: 'json',
        method: "GET",
        url: API_URL + "/supplier/" + ELEMENTO_BACK + "/platforms_allowed",
        data: {
            for_edit: for_edit
        }
    }).done(function (response) {
        $("#formPlatformCode").empty();
        if (response.platforms.length > 0) {
            response.platforms.forEach(function (element) {
                $("#formPlatformCode").append("<option value='" + element.PLATFORM_CODE + "'>" + element.NAME + "</option>");
            });
            if (for_edit) {
                $("#formPlatformCode").val(for_edit);
            }
            $("#btnAddPlatform").removeAttr("disabled");
            $("#btnAddPlatform").removeClass("disabled");
            permitir_add = true;
        } else {
            $("#btnAddPlatform").attr("disabled", "disabled");
            $("#btnAddPlatform").addClass("disabled");
            permitir_add = false;
        }
        get_platform($("#formPlatformCode").val());
    }).fail(function (response) {
        show_message_alert(response.responseJSON.error, "danger");
        oTable_dt.ajax.reload(null, false);
    });
}

function save_formulario(tipo) {
    aux_tipo_save = tipo;
    $("#formFormulario").submit();
}

function add_platform() {
    if (permitir_add) {

        if (($("#formAddPlatform").hasClass("api_mode") && (($("#formClientId").val().split(" ").join("") != "" && $("#formClientSecret").val().split(" ").join("") != "" && $("#formApiKey").val().split(" ").join("") == "") || ($("#formClientId").val().split(" ").join("") == "" && $("#formClientSecret").val().split(" ").join("") == "" && $("#formApiKey").val().split(" ").join("") != "")) && $("#formLastDayProcessed").valid()) || !($("#formAddPlatform").hasClass("api_mode"))) {
            $.ajax({
                dataType: 'json',
                method: "POST",
                url: API_URL + "/supplier/" + ELEMENTO_BACK + "/platform/" + $("#formPlatformCode").val(),
                data: {
                    city_code: $("#formCityCode").val(),
                    client_id: $("#formClientId").val(),
                    client_secret: $("#formClientSecret").val(),
                    api_key: $("#formApiKey").val(),
                    last_day_processed: $("#formLastDayProcessed").val(),
                    edit_mode: $("#formEditMode").val(),
                    last_platform_code: $("#formLastPlatformCode").val()
                }
            }).done(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.message, "success");
                oTable_dt.ajax.reload(null, false);
                close_edit_platform()
            }).fail(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.responseJSON.error, "danger");
                oTable_dt.ajax.reload(null, false);
                close_edit_platform()
            });
        } else {
            show_message_alert(t('SMS_REQUIRED_ADD_PLATFORM'), "danger");
        }


    }
}


function edit_platform(product, API_KEY, CITY_CODE, CITY_NAME, CLIENT_ID, CLIENT_SECRET, LAST_DAY_PROCESSED, PLATFORM_CODE, PLATFORM_NAME) {
    $("#btnAddPlatform").html(t("GUARDAR"));
    $("#btnCloseEditPlatform").css("display", "inline-block");
    get_platforms(PLATFORM_CODE);
    $("#formLastPlatformCode").val(PLATFORM_CODE);
    $("#formEditMode").val("1");
    $("#formCityCode").val(CITY_CODE);
    $("#formClientId").val(CLIENT_ID);
    $("#formClientSecret").val(CLIENT_SECRET);
    $("#formApiKey").val(API_KEY);
    $("#formLastDayProcessed").val(LAST_DAY_PROCESSED);
}

function close_edit_platform() {
    $("#btnAddPlatform").html(t("ANADIR_PLATFORM"));
    $("#btnCloseEditPlatform").css("display", "none");
    get_platforms(false);
    $("#formLastPlatformCode").val("");
    $("#formEditMode").val("0");
    $("#formCityCode").val("");
    $("#formClientId").val("");
    $("#formClientSecret").val("");
    $("#formApiKey").val("");
    $("#formLastDayProcessed").val("");
}

function delete_platform(product, platform) {
    swal.fire({
        title: t('¿ESTAS_SEGURO?'),
        text: t('NO_SE_PUEDE_DESAHACER'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: t('SI_ELIMINAR'),
        cancelButtonText: t('NO_CANCELAR'),
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            KTApp.blockPage({overlayColor: "#000000", state: "primary"});
            $.ajax({
                dataType: 'json',
                method: "DELETE",
                url: API_URL + "/supplier/" + product + "/platform/" + platform
            }).done(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.message, "success");
                oTable_dt.ajax.reload(null, false);
                get_platforms(false);
            }).fail(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.responseJSON.error, "danger");
                oTable_dt.ajax.reload(null, false);
                get_platforms(false);
            });
        } else if (result.dismiss === 'cancel') {

        }
    });
}