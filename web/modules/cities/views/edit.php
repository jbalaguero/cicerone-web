<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    <?= $this->nombre_modulo ?>
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total"><?= $data["elemento"]["CITY_CODE"] ?></span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <a href="<?= ROOTPATH ?><?= MODULO_BACK ?>/list/list" class="btn btn-default btn-bold"><?= $this->t("ATRAS") ?></a>
                <div class="btn-group">
                    <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="btn btn-brand btn-bold">
                        <?= $this->t("GUARDAR") ?>
                    </a>
                    <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_CONTINUAR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_CONTINUAR") ?></span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_CREAR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_CREAR") ?></span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="kt-nav__link">
                                    <span class="kt-nav__link-text"><?= $this->t("GUARDAR_Y_SALIR") ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__body">
                <form id="formFormulario" method="post" action="<?= $data["url_form"] ?>">
                    <div class="kt-form kt-form--label-right">
                        <div class="kt-form__body">
                            <div class="row">
                                <div class="col-2">
                                    <?= $this->fieldGrid("text", $this->t("CODE"), array(
                                        "id" => "formCode",
                                        "name" => "code",
                                        "value" => $data["elemento"]["CITY_CODE"],
                                        "maxlength" => "3",
                                        (ACCION_BACK == "edit" ? "disabled " : "") . "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-8">
                                    <?= $this->fieldGrid("text", $this->t("DESCRIPTION"), array(
                                        "id" => "formDescription",
                                        "name" => "description",
                                        "value" => $data["elemento"]["DESCRIPTION"],
                                        "maxlength" => "50",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-2">
                                    <?= $this->fieldGrid("select", $this->t("COUNTRY"), array(
                                        "id" => "formCountryCode",
                                        "name" => "country_code",
                                        "value" => $data["elemento"]["COUNTRY_CODE"],
                                        "required" => "required",
                                        "options_url" => array(
                                            "url" => API_URL . "/countries",
                                            "index" => "countries",
                                            "value" => "COUNTRY_CODE",
                                            "label" => "DESCRIPTION"
                                        )
                                    )) ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
                <?php
                if(ACCION_BACK == "edit"){
                    ?>
                    <div class="kt-divider">
                        <span></span>
                        <span></span>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <h3><?=$this->t("MAPPINGS")?></h3>
                        </div>
                    </div>
                    <form id="formAddMapping">
                        <div class="row">
                            <div class="col-2">
                                <?= $this->fieldGrid("select", $this->t("SUPPLIER"), array(
                                    "id" => "formSupplierCode",
                                    "name" => "supplier_code"
                                )) ?>
                            </div>
                            <div class="col-2">
                                <?= $this->fieldGrid("text", $this->t("SUPPLIER_CITY_CODE"), array(
                                    "id" => "formSupplierCityCode",
                                    "name" => "supplier_city_code",
                                    "maxlength" => "50",
                                    "required" => "required"
                                )) ?>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label class="col-form-label">&nbsp;</label>
                                    <div>
                                        <a id="btnAddMapping" href="javascript:add_mapping()" class="btn btn-brand btn-bold disabled"><?= $this->t("ANADIR_MAPPING") ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable datatable_script" id="datatable-ajax-<?= MODULO_BACK ?>-<?= ACCION_BACK ?>" data-url="<?= API_URL ?>/city/<?= ELEMENTO_BACK ?>/suppliers">
                        <thead>
                        <tr>
                            <th><?= $this->t("SUPPLIER") ?></th>
                            <th><?= $this->t("CODE") ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>