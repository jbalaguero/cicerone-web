var table_dt = null;
var oTable_dt = null;

jQuery(document).ready(function () {
    table_dt = $('.datatable_script');
    // begin first table

    option_this_datatables = option_comunes_datatables;
    option_this_datatables["order"] = [[ 1, "asc" ]];
    option_this_datatables["ajax"] = {
        "type": "GET",
        "url": table_dt.attr("data-url"),
        "dataSrc": function (json) {
            return json.segments;
        }
    };
    option_this_datatables["columns"] = [
        {'data': 'SEGMENT_CODE'},
        {'data': 'DESCRIPTION'},
        {'data': 'SEGMENT_CODE'}
    ];
    option_this_datatables["columnDefs"] = [
        {
            targets: 0,
            render: function (data, type, full, meta) {
                output = `<div class="kt-user-card-v2"><div class="kt-user-card-v2__details"><a href="` + ROOTPATH + `` + MODULO_BACK + `/edit/` + data + `" class="kt-user-card-v2__email kt-link"><span class="kt-user-card-v2__name">` + data + `</span></a></div></div>`;

                return output;
            },
        },
        {
            targets: -1,
            orderable: false,
            render: function (data, type, full, meta) {
                return `
                        <a href="` + ROOTPATH + `` + MODULO_BACK + `/edit/` + data + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("EDITAR_REGISTRO") + `">
                          <i class="la la-pencil"></i>
                        </a>
                        <a href="javascript:delete_element('` + data + `')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="` + t("ELIMINAR_REGISTRO") + `">
                          <i class="la la-trash"></i>
                        </a>`;
            },
        }
    ];
    oTable_dt = table_dt.DataTable(option_this_datatables);
    var buttons = new $.fn.dataTable.Buttons(oTable_dt, {
        buttons: button_array_buttons
    }).container().prependTo($('.kt-subheader__toolbar'));
});

function delete_element(id) {
    swal.fire({
        title: t('¿ESTAS_SEGURO?'),
        text: t('NO_SE_PUEDE_DESAHACER'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: t('SI_ELIMINAR'),
        cancelButtonText: t('NO_CANCELAR'),
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            KTApp.blockPage({overlayColor: "#000000", state: "primary"});
            $.ajax({
                dataType: 'json',
                method: "DELETE",
                url: API_URL + "/segment/" + id
            }).done(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.message, "success");
                oTable_dt.ajax.reload(null, false);
            }).fail(function (response) {
                KTApp.unblockPage()
                show_message_alert(response.responseJSON.error, "danger");
                oTable_dt.ajax.reload(null, false);
            });
        } else if (result.dismiss === 'cancel') {

        }
    });
}