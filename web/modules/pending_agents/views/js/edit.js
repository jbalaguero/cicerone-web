var aux_tipo_save = null;
$(document).ready(function () {
    $("#formFormulario").validate({
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
                $(placement).addClass("conError");
            } else {
                error.insertAfter(element);
            }
        },
        rules:{
            supplier_agent_code_new:{
                required: function(){
                    return (($("#formCiceroneAgentCode").val() == "_NEW_AGENT_"));
                }
            }
        },
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        }
    });

    $("#formFormulario").ajaxForm({
        dataType: 'json',
        beforeSubmit: function (data, form) {
            if ($(form).valid()) {
                KTApp.blockPage({overlayColor: "#000000", state: "primary"});
                return true;
            } else {
                return false;
            }
        },
        success: function (responseJSON, type, response, form) {
            KTApp.unblockPage()
            show_message_alert(responseJSON.message, 'success');
            if (aux_tipo_save == "GUARDAR_Y_SALIR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/list/list";
            } else if (aux_tipo_save == "GUARDAR_Y_CONTINUAR") {
                document.location.href = ROOTPATH + MODULO_BACK + "/edit/" + ELEMENTO_BACK;
            }
        },
        error: function (data) {
            KTApp.unblockPage()
            show_message_alert(data.responseJSON.error, 'danger');
        }
    });

    $("#formCiceroneAgentCode").change(function(){
        if($(this).val()=="_NEW_AGENT_"){
            $("#formNewCode").slideDown()
        }else{
            KTApp.blockPage();
            $.ajax({
                dataType: 'json',
                method: "GET",
                url: API_URL + "/agent/"+$(this).val()
            }).done(function (response) {
                KTApp.unblockPage()
                $("#formNewCode").slideUp()
            }).fail(function (response) {
                $(this).val("_NEW_AGENT_");
                $("#formCiceroneAgentCode").change();
                KTApp.unblockPage();

            });
        }
    });
    $("#formCiceroneAgentCode").change();
});
function save_formulario(tipo) {
    aux_tipo_save = tipo;
    $("#formFormulario").submit();
}