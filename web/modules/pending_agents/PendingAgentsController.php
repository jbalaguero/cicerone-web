<?php

namespace Cicerone;

class PendingAgentsController extends Controller
{
    protected $myTools;

    public function load()
    {
        $this->nombre_modulo = "Pending agents";
        $this->myTools = new Tools();

        if ($this->getUser()->checkPermisoText("MODULO_PENDINGAGENTS")) {
            call_user_func_array(array($this, ACCION_BACK . "Action"), func_get_args());
        } else {
            header("Location: " . ROOTPATH . "dashboard");
            die();
        }
    }

    public function listAction()
    {
        $this->js = "<script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/list.js'></script>";
        $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php");
    }

    public function editAction()
    {
        $datosURL=explode(";", ELEMENTO_BACK);
        if($datosURL[0]==""){
            $datosURL[0]="___VACIO___";
        }
        if($datosURL[1]==""){
            $datosURL[1]="___VACIO___";
        }
        $respuesta = $this->myTools->peticionCURL("GET", "/pending_agent/" . implode("/", $datosURL), array());
        if ($respuesta["response"]) {
            $elemento = $respuesta["pending_agent"];
            $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
            $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php", array(
                "elemento" => $elemento,
                "url_form" => API_URL . "/pending_agent/" . ($elemento["SUPPLIER_CODE"] != "" ? $elemento["SUPPLIER_CODE"] : "___VACIO___") . "/" . ($elemento["SUPPLIER_AGENT_CODE"] != "" ? $elemento["SUPPLIER_AGENT_CODE"] : "___VACIO___")
            ));
        } else {
            die("Elemento no encontrado");
        }
    }
}