var aux_tipo_save = null;
$(document).ready(function () {
    $("#formFormulario").validate({
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
                $(placement).addClass("conError");
            } else {
                error.insertAfter(element);
            }
        },
        onfocusout: false,
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        }
    });

    $("#formFormulario").ajaxForm({
        dataType: 'json',
        beforeSubmit: function (data, form) {
            if ($(form).valid()) {
                KTApp.blockPage({overlayColor:"#000000",state:"primary"});
                return true;
            } else {
                return false;
            }
        },
        success: function (responseJSON, type, response, form) {
            KTApp.unblockPage()
            show_message_alert(responseJSON.message, 'success');
        },
        error: function (data) {
            KTApp.unblockPage()
            show_message_alert(data.responseJSON.error, 'danger');
        }
    });

    $("#formAddMapping").ajaxForm({
        dataType: 'json',
        beforeSubmit: function (data, form) {
            if ($(form).valid()) {
                KTApp.blockPage({overlayColor:"#000000",state:"primary"});
                return true;
            } else {
                return false;
            }
        },
        success: function (responseJSON, type, response, form) {
            KTApp.unblockPage()
            document.location.href=responseJSON.message;
        },
        error: function (data) {
            KTApp.unblockPage()
            show_message_alert(data.responseJSON.error, 'danger');
        }
    });

    $("#btnExportar").click(function(){
        $("#formAddMapping").submit();
    });

});

function save_formulario(tipo) {
    aux_tipo_save = tipo;
    $("#formFormulario").submit();
}