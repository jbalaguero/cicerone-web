<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    <?= $this->nombre_modulo ?>
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc"
                          id="kt_subheader_total"><?= $data["elemento"]["COMPANY_CODE"] ?></span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <a href="<?= ROOTPATH ?><?= MODULO_BACK ?>/list/list"
                   class="btn btn-default btn-bold"><?= $this->t("ATRAS") ?></a>
                <div class="btn-group">
                    <a href="javascript:save_formulario('GUARDAR_Y_SALIR')" class="btn btn-brand btn-bold">
                        <?= $this->t("GUARDAR") ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__body">
                <form id="formFormulario" method="post" action="<?= $data["url_form"] ?>">
                    <div class="kt-form kt-form--label-right">
                        <div class="kt-form__body">
                            <div class="row">
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("CODE"), array(
                                        "id" => "formCode",
                                        "name" => "code",
                                        "value" => $data["elemento"]["COMPANY_CODE"],
                                        "maxlength" => "10",
                                        (ACCION_BACK == "edit" ? "disabled " : "") . "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("CLIENT_ID"), array(
                                        "id" => "formClientId",
                                        "name" => "client_id",
                                        "value" => $data["elemento"]["CLIENT_ID"],
                                        "maxlength" => "128",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("CLIENT_SECRET"), array(
                                        "id" => "formClientSecret",
                                        "name" => "client_secret",
                                        "value" => $data["elemento"]["CLIENT_SECRET"],
                                        "maxlength" => "128",
                                        "required" => "required"
                                    )) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("DM_URL_LOGIN"), array(
                                        "id" => "formDmUrlLogin",
                                        "name" => "dm_url_login",
                                        "value" => $data["elemento"]["DM_URL_LOGIN"],
                                        "maxlength" => "255",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("DM_URL_AUDIT"), array(
                                        "id" => "formDmUrlAudit",
                                        "name" => "dm_url_audit",
                                        "value" => $data["elemento"]["DM_URL_AUDIT"],
                                        "maxlength" => "255",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("API_CODE"), array(
                                        "id" => "formApiCode",
                                        "name" => "api_code",
                                        "value" => $data["elemento"]["API_CODE"],
                                        "maxlength" => "10",
                                        "required" => "required"
                                    )) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-divider">
                        <span></span>
                        <span></span>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <h3><?= $this->t("SMTP") ?></h3>
                        </div>
                    </div>
                    <div class="kt-form kt-form--label-right">
                        <div class="kt-form__body">
                            <div class="row">
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("SMTP_SERVER"), array(
                                        "id" => "formSmtpServer",
                                        "name" => "smtp_server",
                                        "value" => $data["elemento"]["SMTP_SERVER"],
                                        "maxlength" => "80",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("SMTP_PORT"), array(
                                        "id" => "formSmtpPort",
                                        "name" => "smtp_port",
                                        "value" => $data["elemento"]["SMTP_PORT"],
                                        "maxlength" => "5",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("SMTP_FROM"), array(
                                        "id" => "formSmtpFrom",
                                        "name" => "smtp_from",
                                        "value" => $data["elemento"]["SMTP_FROM"],
                                        "maxlength" => "80",
                                        "required" => "required"
                                    )) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("SMTP_TO"), array(
                                        "id" => "formSmtpTo",
                                        "name" => "smtp_to",
                                        "value" => $data["elemento"]["SMTP_TO"],
                                        "maxlength" => "50",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("SMTP_USER"), array(
                                        "id" => "formSmtpUser",
                                        "name" => "smtp_user",
                                        "value" => $data["elemento"]["SMTP_USER"],
                                        "maxlength" => "50",
                                        "required" => "required"
                                    )) ?>
                                </div>
                                <div class="col-4">
                                    <?= $this->fieldGrid("text", $this->t("SMTP_PASSWORD"), array(
                                        "id" => "formSmtpPassword",
                                        "name" => "smtp_password",
                                        "value" => $data["elemento"]["SMTP_PASSWORD"],
                                        "maxlength" => "50",
                                        "required" => "required"
                                    )) ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
                <?php
                if (ACCION_BACK == "edit") {
                    ?>
                    <div class="kt-divider">
                        <span></span>
                        <span></span>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <h3><?= $this->t("EXPORTAR") ?></h3>
                        </div>
                    </div>
                    <form id="formAddMapping" method="post" action="<?= API_URL ?>/export">
                        <div class="row">
                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("SUPPLIERS"), array(
                                    "id" => "formExportSuppliers",
                                    "name" => "export_suppliers"
                                )) ?>
                            </div>

                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("AGENTS"), array(
                                    "id" => "formExportAgents",
                                    "name" => "export_agents"
                                )) ?>
                            </div>

                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("COUNTRIES"), array(
                                    "id" => "formExportCountries",
                                    "name" => "export_countries"
                                )) ?>
                            </div>

                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("NATIONALITIES"), array(
                                    "id" => "formExportNationalities",
                                    "name" => "export_nationalities"
                                )) ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("CITIES"), array(
                                    "id" => "formExportCities",
                                    "name" => "export_cities"
                                )) ?>
                            </div>
                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("CURRENCIES"), array(
                                    "id" => "formExportCurrencies",
                                    "name" => "export_currencies"
                                )) ?>
                            </div>
                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("SEGMENTS"), array(
                                    "id" => "formExportSegments",
                                    "name" => "export_segments"
                                )) ?>
                            </div>
                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("SUBSEGMENTS"), array(
                                    "id" => "formExportSubsegments",
                                    "name" => "export_subsegments"
                                )) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <?= $this->fieldGrid("checkbox", $this->t("PRODUCTS"), array(
                                    "id" => "formExportProducts",
                                    "name" => "export_products"
                                )) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a id="btnExportar" style="color: white"
                                   class="btn btn-brand btn-bold"><?= $this->t("EXPORTAR") ?></a>
                            </div>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>