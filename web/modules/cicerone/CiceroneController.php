<?php

namespace Cicerone;

class CiceroneController extends Controller
{
    protected $myTools;

    public function load()
    {
        $this->nombre_modulo = "Cicerone";
        $this->myTools = new Tools();

        if ($this->getUser()->checkPermisoText("MODULO_CICERONE")) {
            call_user_func_array(array($this, ACCION_BACK . "Action"), func_get_args());
        } else {
            header("Location: " . ROOTPATH . "dashboard");
            die();
        }
    }

    public function editAction()
    {
        $respuesta = $this->myTools->peticionCURL("GET", "/cicerone/" . $this->getUser()->getCompanyCode(), array());

        if ($respuesta["response"]) {
            $elemento = $respuesta["cicerone"];
            $this->js = "
            <script src='" . ROOTPATH . "modules/" . MODULO_BACK . "/views/js/edit.js'></script>
            ";
            $this->view("include/estructura.php", "modules/" . MODULO_BACK . "/views/" . ACCION_BACK . ".php", array(
                "elemento" => $elemento,
                "url_form" => API_URL . "/cicerone/" . $elemento["COMPANY_CODE"]
            ));
        } else {
            die("Elemento no encontrado");
        }
    }
}