<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <a href="<?=ROOTPATH?>">
                                    <img src="<?=ROOTPATH?>assets/media/company-logos/logo-2.png">
                                </a>
                            </div>

                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">CICERONE</h3>
                                </div>
                                <div class="kt-login__form">
                                    <form id="formLogin" class="kt-form validateThisForm"  action="<?=ROOTPATH?>ajax/login" method="post" data-success="login_success">
                                        <div class="form-group">
                                            <input class="form-control" name="formEmailLogin" id="formEmailLogin" type="text" placeholder="<?=$this->t("CLIENT_ID")?>" autocomplete="off" required/>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control form-control-last" name="formPasswordLogin" id="formPasswordLogin" type="password" placeholder="<?=$this->t("PASSWORD")?>" required />
                                        </div>
                                        <!--<div class="kt-login__extra">
                                            <a href="javascript:;" id="kt_login_forgot"><?=$this->t("OLVIDASTE_PASSWORD")?></a>
                                        </div>-->
                                        <div class="kt-login__actions">
                                            <button id="kt_login_signin_submit_custom" type="submit" class="btn btn-brand btn-pill btn-elevate"><?=$this->t("LOGIN")?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!--<div class="kt-login__forgot">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title"><?=$this->t("RESET_PASSWORD")?></h3>
                                    <div class="kt-login__desc"><?=$this->t("TEXTO_RESET_PASSWORD")?></div>
                                </div>
                                <div class="kt-login__form">
                                    <form id="formResetPassword" class="kt-form validateThisForm" action="<?=ROOTPATH?>ajax/reset-password" method="post" data-success="reset_success">
                                        <div class="form-group">
                                            <input class="form-control" name="formEmailReset" id="formEmailReset" type="text" placeholder="<?=$this->t("CLIENT_ID")?>" autocomplete="off" required />
                                        </div>
                                        <div class="kt-login__actions">
                                            <button id="kt_login_forgot_submit_custom" type="submit" class="btn btn-brand btn-pill btn-elevate"><?=$this->t("RESET")?></button>
                                            <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill"><?=$this->t("CANCEL")?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url(<?=ROOTPATH?>assets/media/bg/bg-4.jpg);">
                <div class="kt-login__section">
                    <div class="kt-login__block">
                        <h3 class="kt-login__title"></h3>
                        <div class="kt-login__desc"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->