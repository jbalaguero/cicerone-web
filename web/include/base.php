<!doctype html>
<html lang="<?= META_IDIOMA_BACKEND ?>">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title>CICERONE</title>
    <meta name="keywords" content="CICERONE"/>
    <meta name="description" content="CICERONE">
    <meta name="author" content="CICERONE">
    <meta name="description" content="CICERONE">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?= ROOTPATH ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/vendors/custom/jquery-ui/jquery-ui.bundle.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/css/style_menu_classic.bundle.css" rel="stylesheet" type="text/css"/>


    <link href="<?= ROOTPATH ?>assets/css/skin_menu_classic/header/base/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/css/skin_menu_classic/header/menu/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/css/skin_menu_classic/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= ROOTPATH ?>assets/css/skin_menu_classic/aside/dark.css" rel="stylesheet" type="text/css"/>

    <link href="<?= ROOTPATH ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="<?= ROOTPATH ?>assets/media/logos/icon.png"/>

    <?= $this->css ?>

    <script>
        var ROOTPATH = "<?=ROOTPATH?>";
        var ROOTPATH_DOMAIN = "<?=ROOTPATH_DOMAIN?>";
        var MODULO_BACK = "<?=MODULO_BACK?>";
        var ACCION_BACK = "<?=ACCION_BACK?>";
        var ELEMENTO_BACK = "<?=ELEMENTO_BACK?>";
        var META_IDIOMA_BACKEND = "<?=META_IDIOMA_BACKEND?>";
        var API_URL = "<?=API_URL?>";
        var TRADUCCIONES = JSON.parse('<?=json_encode($this->getTraducciones(), true)?>');
    </script>

</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading ">
<?php
include $PNDR_RUTA;
?>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#2c77f4",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<!--begin:: Global Mandatory Vendors -->
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery/dist/jquery-migrate-3.1.0.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<!--end:: Global Mandatory Vendors -->
<!--begin:: Global Optional Vendors -->
<script src="<?= ROOTPATH ?>assets/vendors/custom/jquery-ui/jquery-ui.bundle.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-datepicker/dist/locales/bootstrap-datepicker.<?= META_IDIOMA_BACKEND ?>.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery-validation/dist/additional-methods.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/general/jquery-validation/dist/localization/messages_<?= META_IDIOMA_BACKEND ?>.min.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/countdown/jquery.countdown.min.js" type="text/javascript"></script>
<!--end:: Global Optional Vendors -->
<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?= ROOTPATH ?>assets/js/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<script src="<?= ROOTPATH ?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/js/custom.js" type="text/javascript"></script>

<!--begin::Page Vendors(used by this page) -->
<script src="<?= ROOTPATH ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
<script src="<?= ROOTPATH ?>assets/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Scripts(used by this page) -->
<script src="<?= ROOTPATH ?>assets/js/pages/dashboard.js" type="text/javascript"></script>

<!--end::Page Scripts -->

<?= $this->js ?>

</body>
</html>