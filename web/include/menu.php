<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">

    <div id="kt_aside_menu" class="kt-aside-menu  kt-aside-menu--dropdown "
         data-ktmenu-vertical="1"
         data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>dashboard" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-squares"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
            <?php
            if ($this->getUser()->checkPermisoText("MODULO_CICERONE")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>cicerone/edit/edit" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-globe"></i><span class="kt-menu__link-text">Cicerone</span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_PLATFORMS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>platforms/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-computer"></i><span class="kt-menu__link-text"><?=$this->t("PLATFORMS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_SUPPLIERS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>suppliers/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-interface"></i><span class="kt-menu__link-text"><?=$this->t("SUPPLIERS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_AGENTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>agents/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-signs"></i><span class="kt-menu__link-text"><?=$this->t("AGENTS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_PENDINGAGENTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>pending_agents/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i><span class="kt-menu__link-text"><?=$this->t("PENDINGAGENTS")?></span></a></li>
                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_REMOVEDAGENTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>removed_agents/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-trash"></i><span class="kt-menu__link-text"><?=$this->t("REMOVEDAGENTS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_COUNTRIES")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>countries/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-earth-globe"></i><span class="kt-menu__link-text"><?=$this->t("COUNTRIES")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_CITIES")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>cities/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-earth-globe"></i><span class="kt-menu__link-text"><?=$this->t("CITIES")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_CURRENCIES")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>currencies/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-coins"></i><span class="kt-menu__link-text"><?=$this->t("CURRENCIES")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_PERIODS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>periods/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-calendar"></i><span class="kt-menu__link-text"><?=$this->t("PERIODS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_SEGMENTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>segments/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-price-tag"></i><span class="kt-menu__link-text"><?=$this->t("SEGMENTS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_SUBSEGMENTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>subsegments/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-price-tag"></i><span class="kt-menu__link-text"><?=$this->t("SUBSEGMENTS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_PRODUCTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>products/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-star"></i><span class="kt-menu__link-text"><?=$this->t("PRODUCTS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_PENDINGPRODUCTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>pending_products/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i><span class="kt-menu__link-text"><?=$this->t("PENDINGPRODUCTS")?></span></a></li>

                <?php
            }
            if ($this->getUser()->checkPermisoText("MODULO_REMOVEDPRODUCTS")) {
                ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= ROOTPATH ?>removed_products/list/list" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-trash"></i><span class="kt-menu__link-text"><?=$this->t("REMOVEDPRODUCTS")?></span></a></li>

                <?php
            }
            ?>
        </ul>
    </div>
</div>