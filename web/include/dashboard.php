<!--Begin::Dashboard 3-->
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding-top: 0">
        <!-- begin:: Hero -->
        <div class="kt-sc" style="background-image: url('/assets/media/bg/450.jpg')">
            <div class="kt-container ">
                <div class="kt-sc__bottom">
                    <h3 class="kt-sc__heading kt-heading kt-heading--center kt-heading--xxl kt-heading--medium" style="color: white">
                        Bienvenidos a CICERONE
                    </h3>
                </div>
            </div>
        </div>
        <!-- end:: Hero -->
    </div>
</div>
<!--End::Dashboard 3-->