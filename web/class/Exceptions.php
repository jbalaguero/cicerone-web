<?php


namespace Cicerone;

class Exceptions extends \Exception
{
    /* Definimos el listado de errores */
    protected $errorList=array(
    );



    // representación de cadena personalizada del objeto
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}