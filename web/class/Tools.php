<?php

namespace Cicerone;


class Tools
{
    function findExtension($filename)
    {
        $filename = strtolower($filename);
        $exts = explode(".", $filename);
        $n = count($exts) - 1;
        $exts = $exts[$n];
        return $exts;
    }

    function peticionCURL($method, $url, $datos)
    {
        $curl = curl_init();
        if (!$curl) {
            die("Couldn't initialize a cURL handle");
        }

        $datosURL = explode("/", $url);

        $cont=0;
        foreach ($datosURL as $datoURL){
            $datosURL[$cont]=str_ireplace(" ", "___SPACE___", str_ireplace("+", "___PLUS___", $datoURL));
            $cont++;
        }

        curl_setopt($curl, CURLOPT_URL, API_URL.implode("/", $datosURL));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datos);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $apiResponse = curl_exec($curl);

        // Check if any error has occurred
        if (curl_errno($curl))
        {
            die ('cURL error: ' . curl_error($curl));
        }
        else
        {
            $jsonArrayResponse = json_decode($apiResponse, true);
        }

        curl_close($curl);
        return $jsonArrayResponse;
    }

}