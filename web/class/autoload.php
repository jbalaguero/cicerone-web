<?php
require "Controller.php";
require "UsuarioRegistrado.php";
require "Tools.php";
require __DIR__."/../modules/segments/SegmentsController.php";
require __DIR__."/../modules/subsegments/SubsegmentsController.php";
require __DIR__."/../modules/currencies/CurrenciesController.php";
require __DIR__."/../modules/countries/CountriesController.php";
require __DIR__."/../modules/agents/AgentsController.php";
require __DIR__."/../modules/platforms/PlatformsController.php";
require __DIR__."/../modules/suppliers/SuppliersController.php";
require __DIR__."/../modules/cities/CitiesController.php";
require __DIR__."/../modules/periods/PeriodsController.php";
require __DIR__."/../modules/products/ProductsController.php";
require __DIR__."/../modules/pending_products/PendingProductsController.php";
require __DIR__."/../modules/removed_products/RemovedProductsController.php";
require __DIR__."/../modules/cicerone/CiceroneController.php";
require __DIR__."/../modules/pending_agents/PendingAgentsController.php";
require __DIR__."/../modules/removed_agents/RemovedAgentsController.php";
require "Exceptions.php";
require "vendors/PHPExcel/PHPExcel.php";
require "vendors/PHPMailer/PHPMailerAutoload.php";

if (!function_exists('array_key_first')) {
    function array_key_first(array $arr) {
        foreach($arr as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}
