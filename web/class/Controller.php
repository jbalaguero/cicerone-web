<?php

namespace Cicerone;

use PHPMailer;

class Controller
{
    /*Objeto USER*/
    protected $USER;

    protected $traducciones;

    protected $nombre_modulo = "";
    protected $css = "";
    protected $js = "";
    protected $myTools;

    public function __construct()
    {
    }

    /*Es establece el objeto USER*/
    public function setUser(\Cicerone\UsuarioRegistrado $PNDR_USER)
    {
        $this->USER = $PNDR_USER;
    }

    public function setTraducciones($traducciones)
    {
        $this->traducciones = $traducciones;
    }

    public function getTraducciones()
    {
        return $this->traducciones;
    }

    public function getUser()
    {
        return $this->USER;
    }

    public function t($ID)
    {
        if (isset($this->traducciones[$ID])) {
            return $this->traducciones[$ID];
        } else {
            return "<span style='color: red'>".$ID."</span>";
        }
    }

    /* Inicia Cicerone */
    function load()
    {
        $this->myTools = new Tools();

        if (file_exists(ROOTFULLPATH . "lang/" . META_IDIOMA_BACKEND . ".json")) {
            $this->traducciones = json_decode(file_get_contents(ROOTFULLPATH . "lang/" . META_IDIOMA_BACKEND . ".json"), true);
        }

        if (defined("MODULO_BACK")) {
            if (MODULO_BACK == "dashboard") {
                if ($this->esta_logueado()) {
                    $this->dashboardAction();
                } else {
                    /* AL DASHBOARD */
                    header("Location: " . ROOTPATH . "login");
                    die();
                }
            } else if (MODULO_BACK == "login") {
                if ($this->esta_logueado()) {
                    /* AL DASHBOARD */
                    header("Location: " . ROOTPATH . "dashboard");
                    die();
                } else {
                    $this->loginAction();
                }
            } elseif (MODULO_BACK == "logout") {
                $this->logoutAction();
            } elseif (MODULO_BACK == "reset-password") {
                if ($this->esta_logueado()) {
                    /* AL DASHBOARD */
                    header("Location: " . ROOTPATH . "dashboard");
                    die();
                } else {
                    $this->resetPasswordAction();
                }
            } elseif (MODULO_BACK == "validate") {

            } else {
                if ($this->esta_logueado()) {
                    if (file_exists(ROOTFULLPATH . "modules/" . MODULO_BACK . "/index.php")) {
                        include "modules/" . MODULO_BACK . "/index.php";
                    } else {
                        header("HTTP/1.1 401 Fail");
                        die();
                    }
                } else {
                    header("Location: " . ROOTPATH . "login");
                    die();
                }
            }
        } else {
            if ($this->esta_logueado()) {
                /* AL DASHBOARD */
                header("Location: " . ROOTPATH . "dashboard");
                die();
            } else {
                header("Location: " . ROOTPATH . "login");
                die();
            }
        }
    }

    /* Devuelve true si el usuario esta logueado y false si no */
    function esta_logueado()
    {
        return $this->USER->getLogueado();
    }

    function loginAction()
    {
        if (AJAX_BACK) { /* Si estamos en ajax hacemos el proceso de login */

            $login = $this->myTools->peticionCURL("POST", "/login", array(
                "user" => $_POST["formEmailLogin"],
                "password" => $_POST["formPasswordLogin"]
            ));

            if ($login["response"]) {
                $_SESSION["USUARIO"] = $login["user"];
                header('Content-Type: application/json');
                echo json_encode(array(
                    "response" => true
                ), true);
            } else {
                header("HTTP/1.1 401 Fail");
                header('Content-Type: application/json');
                echo json_encode(array(
                    "response" => false,
                    "error" => $login["error"]
                ), true);
            }
            die();
        } else {  /* Si NO estamos en ajax cargamos la vista */
            $this->js = "
                <script src='" . ROOTPATH . "assets/js/pages/login/login-general-custom.js' type='text/javascript'></script>
            ";
            $this->css = "
                <link href='" . ROOTPATH . "assets/css/pages/general/login/login-6.css' rel='stylesheet' type='text/css' />
            ";
            $this->view("include/login.php");
        }
    }

    function dashboardAction()
    {
        $this->view("include/estructura.php", "include/dashboard.php");
    }

    function resetPasswordAction()
    {
        if (AJAX_BACK) {

            if ($elemento = $this->getBBDD()->findOneBy("usuarios", array("LOWER(email)" => strtolower($_POST["formEmailReset"]), "estado" => "CREADO"))) {

                $newPass = $this->generateRandomString();

                $mail = new PHPMailer;

                if (MAIL_SMTP) {
                    $mail->isSMTP();
                }

                $mail->Host = MAIL_HOST;
                $mail->SMTPAuth = MAIL_SMTP_AUTH;
                $mail->Username = MAIL_USER;
                $mail->Password = MAIL_PASS;
                $mail->addCustomHeader('X-MC-Subaccount', 'montero-aramburu');
                $mail->SMTPSecure = MAIL_SMTP_SECURE;
                $mail->Port = MAIL_PORT;

                $mail->setFrom("noreply@montero-aramburu.com", "NOREPLY Montero Aramburu");
                $mail->addReplyTo("noreply@montero-aramburu.com", "NOREPLY Montero Aramburu");

                $mail->isHTML(true);

                $asuntoAEnviar = "Reset Password";

                $mail->Subject = $asuntoAEnviar;
                $mail->AltBody = $asuntoAEnviar;

                $elBody = "
                    <p>Su nueva contraseña es: " . $newPass . "</p>
                ";

                $mail->Body = $elBody;
                $mail->CharSet = 'UTF-8';

                $mail->addAddress($elemento["email"], $elemento["nombre"]);

                if ($mail->send()) {

                    $datoActualizar = array(
                        "password" => md5($newPass),
                        "fecha_modificacion" => time()
                    );
                    $elementoActualizar = array(
                        "id" => $elemento["id"]
                    );

                    if ($this->getBBDD()->modifyWhere("usuarios", $datoActualizar, $elementoActualizar)) {
                        header('Content-Type: application/json');
                        echo json_encode(array(
                            "response" => true,
                            "error" => "Le hemos enviado un mail con los datos necesarios para resetear su contraseña"
                        ), true);
                    } else {
                        header("HTTP/1.1 401 Fail");
                        header('Content-Type: application/json');
                        echo json_encode(array(
                            "response" => false,
                            "error" => "Se ha producido un error"
                        ), true);
                    }
                } else {
                    header("HTTP/1.1 401 Fail");
                    header('Content-Type: application/json');
                    echo json_encode(array(
                        "response" => false,
                        "error" => "Se ha producido un error: " . $mail->ErrorInfo
                    ), true);
                }
            } else {
                header("HTTP/1.1 401 Fail");
                header('Content-Type: application/json');
                echo json_encode(array(
                    "response" => false,
                    "error" => "Se ha producido un error"
                ), true);
            }
            die();
        } else {
            die();
        }
    }

    function logoutAction()
    {
        unset($_SESSION["USUARIO"]);
        header("Location: " . ROOTPATH . "login");
    }

    function view($ruta, $subruta = "", $data = array())
    {
        $PNDR_RUTA = $ruta;
        $PNDR_SUBRUTA = $subruta;
        include "include/base.php";
    }

    function viewHtml($include, $data = array())
    {
        include $include;
    }


    function encripta_for_send($message)
    {
        return str_replace(array("'", '%'), array('%27', "_p0r_"), urlencode(openssl_encrypt($message, "AES-128-ECB", KEY_SEED)));
    }

    function desencripta_for_send($encrypted)
    {
        return openssl_decrypt(urldecode(str_ireplace("_p0r_", '%', $encrypted)), "AES-128-ECB", KEY_SEED);
    }

    function getLangMenu()
    {
        $result = "
                <div class='kt-header__topbar-item kt-header__topbar-item--langs'>
                        <div class='kt-header__topbar-wrapper' data-toggle='dropdown' data-offset='10px,0px'>
                            <span class='kt-header__topbar-icon'>
                            " . strtoupper(META_IDIOMA_BACKEND) . "
                            </span>
                        </div>
                        <div class='dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround'>
                            <ul class='kt-nav kt-margin-t-10 kt-margin-b-10'>
                                <li class='kt-nav__item " . ((META_IDIOMA_BACKEND == "es") ? "kt-nav__item--active" : "")  . "'>
                                    <a href='?lang=es' class='kt-nav__link'>
                                        <span class='kt-nav__link-text'>Español</span>
                                    </a>
                                </li>
                                <li class='kt-nav__item " . ((META_IDIOMA_BACKEND == "en") ? "kt-nav__item--active" : "")  . "'>
                                    <a href='?lang=en' class='kt-nav__link'>
                                        <span class='kt-nav__link-text'>English</span>
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
            ";
        return $result;
    }

    function field($type, $label, $parametros = array())
    {
        if (isset($parametros["id"]) && isset($parametros["name"])) {
            if ($type == "textarea") {
                return $this->fieldTextarea($label, $parametros);
            } elseif ($type == "checkbox") {
                return $this->fieldCheckbox($label, $parametros);
            } elseif ($type == "hidden") {
                return $this->fieldHidden($parametros);
            } elseif ($type == "select") {
                return $this->fieldSelect($label, $parametros);
            } elseif ($type == "radio") {
                return $this->fieldRadio($label, $parametros);
            } elseif ($type == "file") {
                return $this->fieldFile($label, $parametros);
            } else {/*text, number,password, email*/
                return $this->fieldDefault($type, $label, $parametros);
            }
        }
    }

    function fieldGrid($type, $label, $parametros = array())
    {
        if (isset($parametros["id"]) && isset($parametros["name"])) {
            if ($type == "textarea") {
                return $this->fieldTextareaGrid($label, $parametros);
            } elseif ($type == "checkbox") {
                return $this->fieldCheckboxGrid($label, $parametros);
            } elseif ($type == "hidden") {
                return $this->fieldHidden($parametros);
            } elseif ($type == "select") {
                return $this->fieldSelectGrid($label, $parametros);
            } elseif ($type == "radio") {
                return $this->fieldRadioGrid($label, $parametros);
            } elseif ($type == "file") {
                return $this->fieldFileGrid($label, $parametros);
            } else {/*text, number,password, email*/
                return $this->fieldDefaultGrid($type, $label, $parametros);
            }
        }
    }

    function fieldCheckbox($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $checked = "";
        if (isset($parametros["checked"]) && ($parametros["checked"] == "SI" || $parametros["checked"] == 1 || $parametros["checked"] == true)) {
            $checked = " checked ";
        }
        unset($parametros["checked"]);
        $field = "
            <div class='form-group form-group-sm row'>
                <label class='col-xl-3 col-lg-3 col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
                <div class='col-lg-9 col-xl-6'>
                    <span class='kt-switch'>
                        <label>
                            <input type='checkbox' " . $checked . " 
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= "            />
                            <span></span>
                        </label>
                    </span>
                </div>
            </div>
        ";
        return $field;
    }

    function fieldCheckboxGrid($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $checked = "";
        if (isset($parametros["checked"]) && ($parametros["checked"] == "SI" || $parametros["checked"] == 1 || $parametros["checked"] == true)) {
            $checked = " checked ";
        }
        unset($parametros["checked"]);
        $field = "
            <div class='form-group form-group-sm'>
                <label class='col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
                <div>
                    <span class='kt-switch'>
                        <label>
                            <input type='checkbox' " . $checked . " 
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= "            />
                            <span></span>
                        </label>
                    </span>
                </div>
            </div>
        ";
        return $field;
    }

    function fieldRadio($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $checked = "";
        if (isset($parametros["checked"]) && ($parametros["checked"] == "SI" || $parametros["checked"] == 1 || $parametros["checked"] == true)) {
            $checked = " checked ";
        }
        unset($parametros["checked"]);
        $option_name = "";
        if (isset($parametros["option_name"])) {
            $option_name = $parametros["option_name"];
        }
        unset($parametros["option_name"]);
        $field = "
            <div class='form-group form-group-sm row'>
                <label class='col-xl-3 col-lg-3 col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
                <div class='col-lg-9 col-xl-6 col-form-label'>
                   <label class='kt-radio'>
                        <input type='radio' " . $checked . "  
                         ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= "          
                        /> " . $option_name . "
                        <span></span>
                    </label>
                </div>
            </div>
        ";
        return $field;
    }

    function fieldRadioGrid($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $checked = "";
        if (isset($parametros["checked"]) && ($parametros["checked"] == "SI" || $parametros["checked"] == 1 || $parametros["checked"] == true)) {
            $checked = " checked ";
        }
        unset($parametros["checked"]);
        $option_name = "";
        if (isset($parametros["option_name"])) {
            $option_name = $parametros["option_name"];
        }
        unset($parametros["option_name"]);
        $field = "
            <div class='form-group form-group-sm'>
                <label class='col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
                <div>
                    <label class='kt-radio'>
                        <input type='radio' " . $checked . "  
                         ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= "          
                        /> " . $option_name . "
                        <span></span>
                    </label>
                </div>
            </div>
        ";
        return $field;
    }

    function fieldTextarea($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $value = "";
        if (isset($parametros["value"])) {
            $value = $parametros["value"];
        }
        unset($parametros["value"]);
        $field = "
        <div class='form-group row'>
            <label class='col-xl-3 col-lg-3 col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div class='col-lg-9 col-xl-6'>
                <textarea class='form-control " . $clasesExtra . "'
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= ">" . $value . "</textarea>
            </div>
        </div>
        ";

        return $field;
    }

    function fieldTextareaGrid($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $value = "";
        if (isset($parametros["value"])) {
            $value = $parametros["value"];
        }
        unset($parametros["value"]);
        $field = "
        <div class='form-group'>
            <label class='col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div>
                <textarea class='form-control " . $clasesExtra . "'
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= ">" . $value . "</textarea>
            </div>
        </div>
        ";
        return $field;
    }

    function fieldFile($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $value = "";
        if (isset($parametros["value"])) {
            $value = $parametros["value"];
            unset($parametros["required"]);
        }
        unset($parametros["value"]);

        $labelFile = $this->t("ELIGE_ARCHIVO");
        if ($value != "") {
            $labelFile = $this->t("MODIFICAR_ARCHIVO");
        }

        $field = "
        <div class='form-group row'>
            <label class='col-xl-3 col-lg-3 col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div class='col-lg-9 col-xl-6'>
                <div></div>
                <div class='custom-file'>
                    <input type='file' class='custom-file-input " . $clasesExtra . "' ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= " />
                    <label class='custom-file-label' for='" . $parametros["id"] . "'>" . $labelFile . "</label>
                </div>
                
                ";

        if ($value != "") {
            if (in_array($this->myTools->findExtension($value), array("png", "jpg", "jpeg"))) {
                $field .= "
                <div class='mt-3'>
                    <img style='max-width: 100%' src='" . ROOTPATH . "../" . $value . "' >
                </div>
                ";
            } else {
                $field .= "
                <div class='mt-3'>
                    <a target='_blank' href='" . ROOTPATH . "../" . $value . "' class='btn btn-brand' ><i class='fa fa-download' ></i> Descargar Archivo Actual</a>
                </div>
                ";
            }
            $field .= "
                <div class='mt-3'>
                <label class='kt-switch'>
                        <input name='" . $parametros["id"] . "_eli' type='checkbox' 
                         ";

            $field .= "          
                        /> Eliminar archivo actual
                        <span></span>
                    </label>
                </div>
                ";
        }

        $field .= "
            </div>
        </div>
        ";
        return $field;
    }

    function fieldFileGrid($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $value = "";
        if (isset($parametros["value"])) {
            $value = $parametros["value"];
            unset($parametros["required"]);
        }
        unset($parametros["value"]);

        $labelFile = $this->t("ELIGE_ARCHIVO");
        if ($value != "") {
            $labelFile = $this->t("MODIFICAR_ARCHIVO");
        }

        $field = "
        <div class='form-group'>
            <label class='col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div>
                <div></div>
                <div class='custom-file'>
                    <input type='file' class='custom-file-input " . $clasesExtra . "' id='customFile' ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= " />
                    <label class='custom-file-label' for='customFile'>" . $labelFile . "</label>
                </div>
                ";

        if ($value != "") {
            if (in_array($this->myTools->findExtension($value), array("png", "jpg", "jpeg"))) {
                $field .= "
                <div class='mt-3'>
                    <img style='max-width: 100%' src='" . ROOTPATH . "../" . $value . "' >
                </div>
                ";
            } else {
                $field .= "
                <div class='mt-3'>
                    <a target='_blank' href='" . ROOTPATH . "../" . $value . "' class='btn btn-brand' ><i class='fa fa-download' ></i> Descargar Archivo Actual</a>
                </div>
                ";
            }
            $field .= "
                <div class='mt-3'>
                <label class='kt-switch'>
                        <input name='" . $parametros["id"] . "_eli' type='checkbox' 
                         ";

            $field .= "          
                        /> Eliminar archivo actual
                        <span></span>
                    </label>
                </div>
                ";
        }

        $field .= "
            </div>
        </div>
        ";
        return $field;
    }

    function fieldDefault($type, $label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $field = "
        <div class='form-group row'>
            <label class='col-xl-3 col-lg-3 col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div class='col-lg-9 col-xl-6'>
                <input type='" . $type . "' class='form-control " . $clasesExtra . "'
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . str_ireplace("'", "&apos;", $valor) . "' ";
        }

        $field .= " />
            </div>
        </div>
        ";
        return $field;
    }

    function fieldDefaultGrid($type, $label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $field = "
        <div class='form-group'>
            <label class='col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div>
                <input type='" . $type . "' class='form-control " . $clasesExtra . "'
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . str_ireplace("'", "&apos;", $valor) . "' ";
        }

        $field .= " />
            </div>
        </div>
        ";
        return $field;
    }

    function fieldSelect($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $options = "";
        if (isset($parametros["options"])) {
            $options = $parametros["options"];
        }
        unset($parametros["options"]);
        $options_bbdd = "";
        if (isset($parametros["options_bbdd"])) {
            $options_bbdd = $parametros["options_bbdd"];
        }
        unset($parametros["options_bbdd"]);
        $options_url = "";
        if (isset($parametros["options_url"])) {
            $options_url = $parametros["options_url"];
        }
        unset($parametros["options_url"]);
        $value = "";
        if (isset($parametros["value"])) {
            $value = $parametros["value"];
        } elseif (isset($parametros["multiple"])) {
            $value = array();
        }
        unset($parametros["value"]);
        $field = "
        <div class='form-group row'>
            <label class='col-xl-3 col-lg-3 col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div class='col-lg-9 col-xl-6'>
            
                <select  class='form-control " . $clasesExtra . "'
                    ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= ">
                    ";

        if ($options != "") {
            foreach ($options as $valor => $label) {
                $selected = "";

                if (isset($parametros["multiple"])) {
                    if (in_array($valor, $value)) {
                        $selected = "selected";
                    }
                } else {
                    if ($value == $valor) {
                        $selected = "selected";
                    }
                }

                $field .= " <option value='" . str_ireplace("'", "&apos;", $valor) . "' " . $selected . " >" . $label . "</option>";
            }
        }
        if ($options_bbdd != "") {
            if ($datos = $this->getBBDD()->findBy($options_bbdd["tabla"], $options_bbdd["criterio"], $options_bbdd["orden"], $options_bbdd["inicio"], $options_bbdd["cantidad"])) {
                foreach ($datos as $dato) {
                    $selected = "";

                    if (isset($parametros["multiple"])) {
                        if (in_array($dato["id"], $value)) {
                            $selected = "selected";
                        }
                    } else {
                        if ($value == $dato["id"]) {
                            $selected = "selected";
                        }
                    }

                    $field .= " <option value='" . $dato["id"] . "' " . $selected . " >" . $dato["nombre"] . "</option>";
                }
            }
        }
        if ($options_url != "") {
            try {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $options_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $options = json_decode(curl_exec($ch), true);
                curl_close($ch);
                foreach ($options as $valor => $label) {
                    $selected = "";

                    if (isset($parametros["multiple"])) {

                        /* var_dump($valor);
                         var_dump($label);
                         var_dump($value);
                         die();*/

                        if (in_array($valor, $value)) {
                            $selected = "selected";
                        }
                    } else {
                        if ($value == $valor) {
                            $selected = "selected";
                        }
                    }

                    $field .= " <option value='" . str_ireplace("'", "&apos;", $valor) . "' " . $selected . " >" . $label . "</option>";
                }
            } catch (Exceptions $e) {

            }
        }

        $field .= "
                
                </select>
            </div>
        </div>
        ";
        return $field;
    }

    function fieldSelectGrid($label, $parametros = array())
    {
        $clasesExtra = "";
        if (isset($parametros["class"])) {
            $clasesExtra = implode(" ", $parametros["class"]);
        }
        unset($parametros["class"]);
        $options = "";
        if (isset($parametros["options"])) {
            $options = $parametros["options"];
        }
        unset($parametros["options"]);
        $options_bbdd = "";
        if (isset($parametros["options_bbdd"])) {
            $options_bbdd = $parametros["options_bbdd"];
        }
        unset($parametros["options_bbdd"]);
        $options_url = "";
        if (isset($parametros["options_url"])) {
            $options_url = $parametros["options_url"];
        }
        unset($parametros["options_url"]);
        $value = "";
        if (isset($parametros["value"])) {
            $value = $parametros["value"];
        }
        unset($parametros["value"]);
        $field = "
        <div class='form-group'>
            <label class='col-form-label' for='" . $parametros["id"] . "'>" . $label . " " . (isset($parametros["required"]) ? "<span class='required'>*</span>" : "") . "</label>
            <div>
                <select  class='form-control " . $clasesExtra . "'
                    ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . $valor . "' ";
        }

        $field .= ">
                    ";
        if ($options != "") {
            foreach ($options as $valor => $label) {
                $selected = "";

                if (isset($parametros["multiple"])) {
                    if (in_array($valor, $value)) {
                        $selected = "selected";
                    }
                } else {
                    if ($value == $valor) {
                        $selected = "selected";
                    }
                }

                $field .= " <option value='" . str_ireplace("'", "&apos;", $valor) . "' " . $selected . " >" . $label . "</option>";
            }
        }
        if ($options_bbdd != "") {
            if ($datos = $this->getBBDD()->findBy($options_bbdd["tabla"], $options_bbdd["criterio"], $options_bbdd["orden"], $options_bbdd["inicio"], $options_bbdd["cantidad"])) {
                foreach ($datos as $dato) {
                    $selected = "";

                    if (isset($parametros["multiple"])) {
                        if (in_array($dato["id"], $value)) {
                            $selected = "selected";
                        }
                    } else {
                        if ($value == $dato["id"]) {
                            $selected = "selected";
                        }
                    }

                    $field .= " <option value='" . $dato["id"] . "' " . $selected . " >" . $dato["nombre"] . "</option>";
                }
            }
        }

        if ($options_url != "") {
            try {

                if(is_array($options_url)){
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $options_url["url"]);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $options = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    if(isset($options_url["index"])){
                        $options=$options[$options_url["index"]];
                    }
                    foreach ($options as $option) {
                        $selected = "";

                        if (isset($parametros["multiple"])) {
                            if (in_array($option[$options_url["value"]], $value)) {
                                $selected = "selected";
                            }
                        } else {
                            if ($value == $option[$options_url["value"]]) {
                                $selected = "selected";
                            }
                        }

                        $field .= " <option value='" . str_ireplace("'", "&apos;", $option[$options_url["value"]]) . "' " . $selected . " >" . $option[$options_url["label"]] . "</option>";
                    }
                }else{
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $options_url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $options = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    foreach ($options as $valor => $label) {
                        $selected = "";

                        if (isset($parametros["multiple"])) {
                            if (in_array($valor, $value)) {
                                $selected = "selected";
                            }
                        } else {
                            if ($value == $valor) {
                                $selected = "selected";
                            }
                        }

                        $field .= " <option value='" . str_ireplace("'", "&apos;", $valor) . "' " . $selected . " >" . $label . "</option>";
                    }
                }


            } catch (Exceptions $e) {

            }
        }
        $field .= "
                
                </select>
            </div>
        </div>
        ";
        return $field;
    }

    function fieldHidden($parametros = array())
    {
        $field = "
                <input type='hidden' class='form-control'
        ";

        foreach ($parametros as $atributo => $valor) {
            $field .= " " . $atributo . "='" . str_ireplace("'", "&apos;", $valor) . "' ";
        }

        $field .= " />
        ";
        return $field;
    }

    function fechaHoraToUnix($fechaHora)
    {
        $datosFechaHora = explode(" ", $fechaHora);
        $datosFecha = explode("/", $datosFechaHora[0]);
        if (isset($datosFechaHora[1])) {
            $datosHora = explode(":", $datosFechaHora[1]);
            if (isset($datosHora[0])) {
                $horas = $datosHora[0];
            } else {
                $horas = 0;
            }
            if (isset($datosHora[1])) {
                $minutos = $datosHora[1];
            } else {
                $minutos = 0;
            }
            if (isset($datosHora[2])) {
                $segundos = $datosHora[2];
            } else {
                $segundos = 0;
            }
        } else {
            $horas = 0;
            $minutos = 0;
            $segundos = 0;
        }
        return mktime($horas, $minutos, $segundos, $datosFecha[1], $datosFecha[0], $datosFecha[2]);
    }

    function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}