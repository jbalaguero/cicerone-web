$(document).ready(function () {
    $(".validateThisForm").each(function () {
        $("#" + $(this).attr("id")).validate();
        $("#" + $(this).attr("id")).ajaxForm({
            dataType: 'json',
            beforeSubmit: function (data, form) {
                if ($(form).valid()) {
                    return true;
                } else {
                    return false;
                }
            },
            success: function (responseJSON, type, response, form) {
                window[form.data("success")](responseJSON);
            },
            error: function (data) {
                show_message_alert(data.responseJSON.error, 'danger');
            }
        });
    });

    $(".datepicker_field").datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: !0,
        orientation: "bottom left",
        templates: t,
        format: "yyyy/mm/dd",
        language: META_IDIOMA_BACKEND
    });

    set_active_menu();
});

var button_array_buttons = [
    'copyHtml5',
    'excelHtml5'
];

var option_comunes_datatables = {
    language: {
        url: ROOTPATH + "assets/vendors/custom/datatables/i18n/" + META_IDIOMA_BACKEND + ".lang"
    },
    responsive: true,
    searchDelay: 500,
    processing: true,
    stateSave: true,
    aLengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
    iDisplayLength: 25,
    dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>"
};

function show_message_alert(message, type) {

    if (message.substring(0, 2) == "t(") {
        message = t(message.substring(2, (message.length - 1)));
    }

    var content_success = {};

    content_success.message = message;

    var notify_success = $.notify(content_success, {
        type: type,
        allow_dismiss: false,
        newest_on_top: false,
        mouse_over: false,
        showProgressbar: false,
        spacing: 10,
        timer: 2000,
        placement: {
            from: 'top',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 1000,
        z_index: 10000,
        animate: {
            enter: 'animated bounce',
            exit: 'animated bounce'
        }
    });
}

function login_success(responseJSON) {
    document.location.href = ROOTPATH + "dashboard";
}

function reset_success(responseJSON) {
    show_message_alert(responseJSON.error, "success");
}

function set_active_menu() {
    var elementos = [];
    $("#kt_aside_menu_wrapper a[href]").each(function () {
        if (!$(this).hasClass("kt-menu__toggle")) {
            if ($(this).attr("href").split("/")[1] == MODULO_BACK) {
                elementos.push($(this));
            }
        }
    });

    if (elementos.length > 1) {
        var elementos_2 = [];
        elementos.forEach(function (ele) {
            if (ele.attr("href").split("/")[2] == ACCION_BACK) {
                elementos_2.push(ele);
            }
        });
        if (elementos_2.length > 1) {
            var elementos_3 = [];
            elementos_2.forEach(function (ele) {
                if ($(ele).attr("href").split("/")[3] == ELEMENTO_BACK) {
                    elementos_3.push(ele);
                }
            });
            if (elementos_3.length > 1) {
                elementos_3.forEach(function (ele) {
                    elLi = ele.closest("li");
                    elLi.addClass("kt-menu__item--active");

                    if (elLi.parent().closest("li").length > 0) {
                        elLi = elLi.parent().closest("li");
                        elLi.addClass("kt-menu__item--open");
                        elLi.addClass("kt-menu__item--here");
                        if (elLi.parent().closest("li").length > 0) {
                            elLi = elLi.parent().closest("li");
                            elLi.addClass("kt-menu__item--open");
                            elLi.addClass("kt-menu__item--here");
                        }
                    }
                });
            } else {
                elLi = elementos_3[0].closest("li");
                elLi.addClass("kt-menu__item--active");

                if (elLi.parent().closest("li").length > 0) {
                    elLi = elLi.parent().closest("li");
                    elLi.addClass("kt-menu__item--open");
                    elLi.addClass("kt-menu__item--here");
                    if (elLi.parent().closest("li").length > 0) {
                        elLi = elLi.parent().closest("li");
                        elLi.addClass("kt-menu__item--open");
                        elLi.addClass("kt-menu__item--here");
                    }
                }
            }
        } else {
            elLi = elementos_2[0].closest("li");
            elLi.addClass("kt-menu__item--active");

            if (elLi.parent().closest("li").length > 0) {
                elLi = elLi.parent().closest("li");
                elLi.addClass("kt-menu__item--open");
                elLi.addClass("kt-menu__item--here");
                if (elLi.parent().closest("li").length > 0) {
                    elLi = elLi.parent().closest("li");
                    elLi.addClass("kt-menu__item--open");
                    elLi.addClass("kt-menu__item--here");
                }
            }
        }
    } else {
        if (elementos[0] != undefined) {
            elLi = elementos[0].closest("li");
            elLi.addClass("kt-menu__item--active");

            if (elLi.parent().closest("li").length > 0) {
                elLi = elLi.parent().closest("li");
                elLi.addClass("kt-menu__item--open");
                elLi.addClass("kt-menu__item--here");
                if (elLi.parent().closest("li").length > 0) {
                    elLi = elLi.parent().closest("li");
                    elLi.addClass("kt-menu__item--open");
                    elLi.addClass("kt-menu__item--here");
                }
            }
        }

    }
}

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function (a) {
        var x;

        if ($.trim(a) !== '') {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [0, 0, 0];
            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + ((undefined != frTimea[2]) ? frTimea[2] : 0)) * 1;
        } else {
            x = Infinity;
        }

        return x;
    },

    "date-euro-asc": function (a, b) {
        return a - b;
    },

    "date-euro-desc": function (a, b) {
        return b - a;
    }
});

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function nl2br(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function t(ID) {
    if (TRADUCCIONES[ID] === undefined) {
        return "<span style='color: red'>" + ID + "</span>";
    } else {
        return TRADUCCIONES[ID];
    }
}