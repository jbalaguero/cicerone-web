var table_dt = null;
var oTable_dt = null;
jQuery(document).ready(function () {
    table_dt = $('.datatable_script');
    // begin first table
    oTable_dt = table_dt.DataTable({
        language: {
            url: ROOTPATH + "assets/vendors/custom/datatables/i18n/" + META_IDIOMA_BACKEND + ".lang"
        },
        responsive: true,
        searchDelay: 500,
        processing: true,
        stateSave: true,
        ajax: table_dt.attr("data-url"),
        aLengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        iDisplayLength: -1,
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        columnDefs: [
            {
                targets: 0,
                render: function (data, type, full, meta) {
                    output = `<div class="kt-user-card-v2"><div class="kt-user-card-v2__details"><a href="` + ROOTPATH + `` + MODULO_BACK + `/edit/` + data[0] + `" class="kt-user-card-v2__email kt-link"><span class="kt-user-card-v2__name">` + data[1] + `</span></a></div></div>`;

                    return output;
                },
            },
            { type: 'date-euro', targets: -4 },
            { type: 'date-euro', targets: -3 },
            {
                targets: -2,
                render: function (data, type, full, meta) {
                    var status = {
                        "NO": {'title': 'Desactivado', 'state': 'danger'},
                        "SI": {'title': 'Activo', 'state': 'success'}
                    };
                    return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
                        '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
                },
            },
            {
                targets: -1,
                orderable: false,
                render: function (data, type, full, meta) {
                    return `
                        <a href="` + ROOTPATH + `` + MODULO_BACK + `/edit/` + data + `" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                          <i class="la la-pencil"></i>
                        </a>
                        <a href="javascript:delete_element('` + data + `')" class="btn btn-sm btn-clean btn-icon btn-icon-md"  title="Eliminar registro">
                          <i class="la la-trash"></i>
                        </a>`;
                },
            }
        ]
    });
    var buttons = new $.fn.dataTable.Buttons(oTable_dt, {
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'colvis'
        ]
    }).container().prependTo($('.kt-subheader__toolbar'));
});

function delete_element(id) {
    swal.fire({
        title: '¿Estas seguro?',
        text: "No podrás deshacer esta acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar',
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                dataType: 'json',
                method: "POST",
                url: ROOTPATH + "ajax/" + MODULO_BACK + "/delete/" + id
            }).done(function (response) {
                show_message_alert(response.mensaje, "success");
                oTable_dt.ajax.reload(null, false);
            }).fail(function (response) {
                show_message_alert(response.responseJSON.error, "danger");
                oTable_dt.ajax.reload(null, false);
            });
        } else if (result.dismiss === 'cancel') {

        }
    });
}