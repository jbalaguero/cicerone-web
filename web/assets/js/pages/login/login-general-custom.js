var login = $('#kt_login');
jQuery(document).ready(function() {
    $('#kt_login_forgot').click(function(e) {
        e.preventDefault();
        login.removeClass('kt-login--signin');

        login.addClass('kt-login--forgot');
        KTUtil.animateClass(login.find('.kt-login__forgot')[0], 'flipInX animated');
    });

    $('#kt_login_forgot_cancel').click(function(e) {
        e.preventDefault();
        login.removeClass('kt-login--forgot');

        login.addClass('kt-login--signin');
        KTUtil.animateClass(login.find('.kt-login__signin')[0], 'flipInX animated');
    });
});