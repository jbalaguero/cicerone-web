<?php
session_start();

require "class/autoload.php";

$PNDR_CONTROLLER = new \Cicerone\Controller();

include "defines.php";

if(isset($_GET["lang"])){
    if(file_exists(ROOTFULLPATH."lang/".$_GET["lang"].".json")){
        /* Si existe el GET lang y el archivo lang esta creado, metemos el lang en session y recargamos */
        $_SESSION["PNDR_META_IDIOMA_BACKEND"]=$_GET["lang"];
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        header("Location: " . explode("?", $actual_link)[0]);
        die();
    }else{
        die("Idioma no encontrado");
    }
} elseif(!isset($_SESSION["PNDR_META_IDIOMA_BACKEND"])){
    if(file_exists(ROOTFULLPATH."lang/".META_IDIOMA_BACKEND.".json")){
        /* Si NO existe el GET lang y NO existe el SESSION de lang lo creamos y recargamos */
        $_SESSION["PNDR_META_IDIOMA_BACKEND"]=META_IDIOMA_BACKEND;
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        header("Location: " . explode("?", $actual_link)[0]);
        die();
    }
}
$PNDR_USER = new \Cicerone\UsuarioRegistrado();
$PNDR_USER->setUser();
$PNDR_CONTROLLER->setUser($PNDR_USER);
$PNDR_CONTROLLER->load();
die();